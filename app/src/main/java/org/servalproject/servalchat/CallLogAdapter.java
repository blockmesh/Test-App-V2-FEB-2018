package org.servalproject.servalchat;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class CallLogAdapter extends BaseAdapter{
    private Context mContext;
    private ArrayList<CallLogItemList> callLogItemLists;
    private Boolean type;
    public CallLogAdapter(Context context, ArrayList<CallLogItemList> callLogItemList, Boolean type) {
        mContext = context;
        this.callLogItemLists = callLogItemList;
        this.type= type;
    }

    @Override
    public int getCount() {
        return callLogItemLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String appPackageName = mContext.getPackageName();
        if (convertView == null) {



            grid = new View(mContext);
            grid = inflater.inflate(R.layout.call_log_list, null);

            ImageView  imgCallType = (ImageView) grid.findViewById(R.id.img_call_type);

            TextView  tvNumber = (TextView) grid.findViewById(R.id.tv_call_number);
            TextView tvName = (TextView) grid.findViewById(R.id.tv_call_name);
            TextView tvTime = (TextView) grid.findViewById(R.id.tv_time);

            tvName.setText(callLogItemLists.get(position).getName());
            tvNumber.setText(callLogItemLists.get(position).getPhNumber());

            int callTypeId =0;
            if(callLogItemLists.get(position).getDir().equalsIgnoreCase("OUTGOING")) {


                callTypeId = mContext.getResources().getIdentifier(appPackageName + ":drawable/outgoingcall" , null, null);

            }
            else if(callLogItemLists.get(position).getDir().equalsIgnoreCase("INCOMING"))
            {
                callTypeId = mContext.getResources().getIdentifier(appPackageName + ":drawable/incomeingcall" , null, null);

            }
            else if(callLogItemLists.get(position).getDir().equalsIgnoreCase("MISSED"))
            {
                callTypeId = mContext.getResources().getIdentifier(appPackageName + ":drawable/misscall" , null, null);

            }
            imgCallType.setImageResource(callTypeId);
            tvTime.setText(callLogItemLists.get(position).getCallDayTime());

        } else {
            grid = (View) convertView;
            ImageView  imgCallType = (ImageView) grid.findViewById(R.id.img_call_type);

            TextView  tvNumber = (TextView) grid.findViewById(R.id.tv_call_number);
            TextView tvName = (TextView) grid.findViewById(R.id.tv_call_name);
            TextView tvTime = (TextView) grid.findViewById(R.id.tv_time);

            tvName.setText(callLogItemLists.get(position).getName());
            tvNumber.setText(callLogItemLists.get(position).getPhNumber());

            int callTypeId =0;
            if(callLogItemLists.get(position).getDir().equalsIgnoreCase("OUTGOING")) {


                callTypeId = mContext.getResources().getIdentifier(appPackageName + ":drawable/outgoingcall" , null, null);

            }
            else if(callLogItemLists.get(position).getDir().equalsIgnoreCase("INCOMING"))
            {
                callTypeId = mContext.getResources().getIdentifier(appPackageName + ":drawable/incomeingcall" , null, null);

            }
            else if(callLogItemLists.get(position).getDir().equalsIgnoreCase("MISSED"))
            {
                callTypeId = mContext.getResources().getIdentifier(appPackageName + ":drawable/misscall" , null, null);

            }
            imgCallType.setImageResource(callTypeId);
            tvTime.setText(callLogItemLists.get(position).getCallDayTime());


        }


        return grid;
    }
}