package org.servalproject.servalchat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.servalproject.servalchat.navigation.Events;
import org.servalproject.servalchat.navigation.MainActivity;
import org.servalproject.servalchat.views.BottomNavigationListener;
import org.servalproject.servalchat.views.BottomNavigationViewHelper;
import org.servalproject.servalchat.views.ContactAdapter2;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import agency.tango.android.avatarviewglide.GlideLoader;

/**
 * Fragment version of ContactsActivity
 * Created by rbrauwers on 14/02/18.
 */

public class ContactFragment extends Fragment {
    ImageView imgBack,imgSearch,imgClose;
    TextView txtTitle;
    EditText etSearch;
    LinearLayout llTop;
    //ListView listContacts;
    RecyclerView listContacts;
    ProgressBar progressBar;

    private ArrayList<ContactItemList> contactItemLists = new ArrayList<ContactItemList>();
    private ArrayList<ContactItemList> contactItemLists1 = new ArrayList<ContactItemList>();
    private ArrayList<ContactItemList> contactItemLists2 = new ArrayList<ContactItemList>();
    //private ContactAdapter contactAdapter;
    private ContactAdapter2 contactAdapter;
    CharSequence filterString="";
    Cursor cursor ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_contact_2, container, false);

        double ww = MainActivity.width/3;
        double hh = MainActivity.height/5;

        imgClose = rootView.findViewById(R.id.img_close);
        imgBack = rootView.findViewById(R.id.img_more);
        imgSearch = rootView.findViewById(R.id.img_search);
        txtTitle = rootView.findViewById(R.id.txt_title);
        etSearch = rootView.findViewById(R.id.et_search);
        llTop = rootView.findViewById(R.id.ll_top);
        progressBar = rootView.findViewById(R.id.progressBar);

        // Commented by kine
        //LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,((int)hh/7)*3);
        //llTop.setLayoutParams(parms);

        listContacts = rootView.findViewById(R.id.list_contacts);
        listContacts.setLayoutManager(new LinearLayoutManager(getActivity()));

        RelativeLayout.LayoutParams parmsImgMore = new RelativeLayout.LayoutParams((int)hh/3,(int)hh/3);
        imgBack.setPadding((int)hh/13,0,0,0);
        parmsImgMore.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        parmsImgMore.addRule(RelativeLayout.CENTER_VERTICAL);
        imgBack.setLayoutParams(parmsImgMore);

        RelativeLayout.LayoutParams parmsImgSearch = new RelativeLayout.LayoutParams((int)hh/3,(int)hh/3);
        parmsImgSearch.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        parmsImgSearch.addRule(RelativeLayout.CENTER_VERTICAL);
        imgSearch.setPadding((int)hh/13,0,0,0);
        imgSearch.setLayoutParams(parmsImgSearch);

        RelativeLayout.LayoutParams parmsImgClose = new RelativeLayout.LayoutParams((int)hh/3,(int)hh/3);
        imgClose.setPadding((int)hh/13,0,0,0);
        parmsImgClose.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        parmsImgClose.addRule(RelativeLayout.CENTER_VERTICAL);
        imgClose.setLayoutParams(parmsImgClose);
        imgClose.setVisibility(View.GONE);

        txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)hh/5);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ContactActivity.this.finish();

            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgClose.setVisibility(View.VISIBLE);
                etSearch.setVisibility(View.VISIBLE);
                etSearch.setFocusable(true);
                etSearch.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
                txtTitle.setVisibility(View.GONE);
                imgSearch.setVisibility(View.GONE);

            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgClose.setVisibility(View.GONE);
                etSearch.setVisibility(View.GONE);
                txtTitle.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.VISIBLE);
                etSearch.setText("");
                InputMethodManager imm = (InputMethodManager)
                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);

            }
        });

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getContact();
            }
        });
        thread.start();


        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                filterString = cs;
                contactItemLists.addAll(contactItemLists2);
                Log.e("San ",arg1+" "+arg2+" "+arg3);
                getFilter();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        // TODO KINE
        /*
        listContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String number = contactItemLists.get(position).getPhNumber();

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+number));
                try {
                    startActivity(callIntent);
                }
                catch (SecurityException se)
                {

                }catch (Exception e){}
            }
        });
        */

        return rootView;
    }

    public void getContact() {
        cursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        while (cursor.moveToNext()) {
//
            ContactItemList contactItemList = new ContactItemList();

            contactItemList.setName( cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));

            contactItemList.setPhNumber(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));

            if (!contactItemLists.contains(contactItemList))
                contactItemLists.add(contactItemList);
//            StoreContacts.add(name + " "  + ":" + " " + phonenumber);
        }


        cursor.close();
        try{
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    contactItemLists2.addAll(contactItemLists);
                    contactAdapter  = new ContactAdapter2(contactItemLists, new GlideLoader(""));
                    contactAdapter.notifyDataSetChanged();
                    listContacts.setAdapter(contactAdapter);
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch(Exception exception){
            exception.printStackTrace();
        }
    }

//
//    @Override
//    public Filter getFilter() {
//        // TODO Auto-generated method stub
//        Filter filter = new Filter() {
//
//            @Override
//            protected void publishResults(CharSequence constraint,
//                                          FilterResults results) {
//                // TODO Auto-generated method stub
//                arr = (ArrayList<ContactItemList>) results.values;
//                notifyDataSetChanged();
//            }
//
//            @Override
//            protected FilterResults performFiltering(CharSequence constraint) {
//                // TODO Auto-generated method stub
//                FilterResults results = new FilterResults();
//
//                if (constraint == null || constraint.length() == 0) {
//                    // No filter implemented we return all the list
//                    results.values = contactItemLists;
//                    results.count = contactItemLists.size();
//
//                    isFilter = false;
//
//                } else {
//
//                    FilteredList.clear();
//                    isFilter = true;
//                    Log.e("San"," name");
//                    for (int i = 0; i < contactItemLists.size(); i++) {
//
//                        ContactItemList data1 = contactItemLists.get(i);
//
//
//                        if (data1.name.toString().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
//                            FilteredList.add(data1);
//
//                            Log.e("San",data1.name.toString()+" name");
//                        }
//
//                    }
//
//                    results.values = FilteredList;
//                    results.count = FilteredList.size();
//
//                }
//                return results;
//            }
//        };
//        return filter;
//    }


    public void  getFilter()
    {
        contactItemLists1.clear();
        contactItemLists1.addAll(contactItemLists);

        contactItemLists.clear();
        if(filterString.toString().length() >0 ) {
            for (int i = 0; i < contactItemLists1.size(); i++) {

                ContactItemList data1 = contactItemLists1.get(i);

//            Log.e("San","contactItemLists1:- "+contactItemLists1.size());
                if (data1.name.toString().toLowerCase().contains(filterString.toString().toLowerCase())) {
//            if (data1.name.toString().startsWith(filterString.toString())) {
                    contactItemLists.add(data1);
//                Log.e("San",data1.name.toString()+" name");
                }

            }
        }
        else
        {
            contactItemLists.addAll(contactItemLists2);
        }
//        Log.e("San","size"+contactItemLists.size());
        contactAdapter.notifyDataSetChanged();
//        listContacts.setAdapter(contactAdapter);
    }
}