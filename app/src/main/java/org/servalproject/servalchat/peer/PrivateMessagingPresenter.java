package org.servalproject.servalchat.peer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.servalproject.mid.Identity;
import org.servalproject.mid.MessageList;
import org.servalproject.mid.Peer;
import org.servalproject.servalchat.App;
import org.servalproject.servalchat.R;
import org.servalproject.servalchat.navigation.MainActivity;
import org.servalproject.servalchat.views.BackgroundWorker;
import org.servalproject.servalchat.views.BasicViewHolder;
import org.servalproject.servalchat.views.Presenter;
import org.servalproject.servalchat.views.PresenterFactory;
import org.servalproject.servalchat.views.ScrollingAdapter;
import org.servalproject.servalchat.views.TimestampView;
import org.servalproject.servaldna.ServalDInterfaceException;
import org.servalproject.servaldna.meshms.MeshMSException;
import org.servalproject.servaldna.meshms.MeshMSMessage;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by jeremy on 27/07/16.
 */
public final class PrivateMessagingPresenter extends Presenter<PrivateMessaging> {
	private MessageList messages;
	private final Peer peer;
	private static final String TAG = "PrivateMessaging";
	private boolean sending = false;
	public ScrollingAdapter<MeshMSMessage, ItemHolder> adapter;
	private MeshMSMessage delivered;
	String prefPeerLastMsg="";
	String prefPeerId="";
	long lastId =0;
	boolean isLastMsg =true;
//	private Context context;
	private PrivateMessagingPresenter(PresenterFactory<PrivateMessaging, ?> factory, String key, Identity identity, Peer peer) {
		super(factory, key, identity);
		this.peer = peer;
	}

	public static PresenterFactory<PrivateMessaging, PrivateMessagingPresenter> factory
			= new PresenterFactory<PrivateMessaging, PrivateMessagingPresenter>() {

		@Override
		protected PrivateMessagingPresenter create(String key, Identity id, Peer peer) {
			return new PrivateMessagingPresenter(this, key, id, peer);
		}
	};

	@Override
	protected void bind(PrivateMessaging view) {
		Log.d(TAG, "bind");
		view.send.setEnabled(!sending);
		view.list.setAdapter(adapter);
		if (App.isTesting() && "".equals(view.message.getText().toString()))
			view.message.setText("Sample Message \uD83D\uDE0E;");
	}

	@Override
	protected void restore(Bundle config) {
		Log.d(TAG, "restore");

		// TODO show peer details?
//		Log.e("San", "PMP identity:- "+identity.toString());
		messages = identity.messaging.getPrivateMessages(peer.getSubscriber());
		prefPeerId =  messages.peer.sid.toString();
//		Log.e("San","PMP messages "+messages.peer.sid);

		//
		prefPeerLastMsg = MainActivity.pref.getString(messages.peer.sid.toString(), null);

		adapter = new ScrollingAdapter<MeshMSMessage, ItemHolder>(messages) {
			@Override
			public ItemHolder create(ViewGroup parent, int viewType) {
				Log.d(TAG, "Adapter::create");
//				Log.e("San","ACK_HERE");
				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
				switch (MeshMSMessage.Type.values()[viewType]){
					default:
//						Log.e("San","ACK_HERE default");
						return new TextItemHolder(inflater, parent, true);
					case MESSAGE_RECEIVED:
//						Log.e("San","ACK_HERE MESSAGE_RECEIVED");
						return new TextItemHolder(inflater, parent, false);
					case ACK_RECEIVED:
//						Log.e("San","ACK_HERE ACK_RECEIVED");
						return new AckHolder(inflater, parent);
				}
			}

			@Override
			protected void addItem(int index, MeshMSMessage item) {
				Log.d(TAG, "Adapter::addItem");
				if (item.type == MeshMSMessage.Type.ACK_RECEIVED) {
					if (delivered!=null)
						items.removeItem(delivered);
					delivered = item;
				}
				super.addItem(index, item);
			}

			@Override
			protected void bind(ItemHolder holder, MeshMSMessage item) {
				Log.d(TAG, "Adapter::bind");
			}

			@Override
			public void insertedItem(MeshMSMessage item, int position) {
				Log.d(TAG, "Adapter::insertedItem");
				super.insertedItem(item, position);
//				Log.e("San","PMP getItemCount "+getItemCount());
				while((++position)+1<getItemCount()){
					if (getItem(position).type != MeshMSMessage.Type.ACK_RECEIVED){
						notifyItemChanged(position);
						break;
					}
				}
			}

			private MeshMSMessage previousMessage(int position){
//				Log.e("San","PMP positionP "+position);
				if(position ==0)
					isLastMsg = true;
				while(--position>=0){
					MeshMSMessage item = getItem(position);
					if (item.type!=MeshMSMessage.Type.ACK_RECEIVED)
						return item;
				}
				return null;
			}
			@Override
			protected void bindItem(ItemHolder holder, int position) {
				Log.d(TAG, "Adapter::bindItem");
				holder.bind(getItem(position), previousMessage(position));
//				Log.e("San","PMP position "+position);
			}

			@Override
			protected int getItemType(MeshMSMessage item) {
				return item.type.ordinal();
			}

			@Override
			protected MainActivity getActivity() {
				PrivateMessaging view = getView();
				return view == null ? null : view.activity;
			}

			@Override
			public long getItemId(int position) {
				MeshMSMessage item = getItem(position);
				if (item == null)
					return Long.MAX_VALUE;
				return item.getId();
			}
		};
		adapter.setHasStableIds(true);
	}

	private void markRead(){
//		Log.e("San","markRead");
		if (!messages.isRead()) {
			new BackgroundWorker() {
				@Override
				protected void onBackGround() throws Exception {
					messages.markRead();
				}

				@Override
				protected void onComplete(Throwable t) {
					if (t==null)
						return;
					PrivateMessaging view = getView();
					if (view!=null)
						view.activity.showError(t);
					else
						rethrow(t);
				}
			}.execute();
		}
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		adapter.clear();
		markRead();
	}

	@Override
	public void onVisible() {
		Log.d(TAG, "onVisible");
		super.onVisible();
		adapter.onVisible();
	}

	@Override
	public void onHidden() {
		Log.d(TAG, "onHidden");
		super.onHidden();
		adapter.onHidden();
	}

	public void send() {
		Log.d(TAG, String.format("send. Visibility: %d", getView().getVisibility()));

		App.isMsgReciever = false;
		App.isMsgSender = true;
		PrivateMessaging view = getView();
		if (view == null)
			return;
		final String message = view.message.getText().toString();
		if ("".equals(message))
			return;

		if (view.activity != null) {
			view.activity.addPeerAsChatter(peer);
		}

		sending = true;
		view.send.setEnabled(false);

		BackgroundWorker sender = new BackgroundWorker() {
			@Override
			protected void onBackGround() throws ServalDInterfaceException, MeshMSException, IOException {
				prefPeerLastMsg = message;

//				MainActivity.editor = MainActivity.pref.edit();
//				MainActivity.editor.putString(prefPeerId, prefPeerLastMsg+"_date_"+ new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.getDefault()).format(new Date()));
//				MainActivity.editor.commit();
				messages.sendMessage(message);
				messages.markRead();
			}

			@Override
			protected void onComplete(Throwable t) {
				Log.d(TAG, "finishSendMessage [1]");

				sending = false;
				PrivateMessaging view = getView();
				if (view!=null) {
					Log.d(TAG, "finishSendMessage [2]");
					view.send.setEnabled(true);
					if (t == null)
						view.message.setText("");
					else
						view.activity.showError(t);
				} else {
					rethrow(t);
				}
			}
		};
		sender.execute();

		view.postDelayed(new Runnable() {
			@Override
			public void run() {
				adapter.notifyDataSetChanged();
			}
		}, 1000);
	}

	public abstract class ItemHolder extends BasicViewHolder {
		public ItemHolder(View itemView) {
			super(itemView);
		}
		public abstract void bind(MeshMSMessage item, MeshMSMessage previous);
	}

	private class TextItemHolder extends ItemHolder{
		TextView message;
		TimestampView age;

		public TextItemHolder(LayoutInflater inflater, ViewGroup parent, boolean myMessage) {
			super(inflater.inflate(myMessage ? R.layout.my_message : R.layout.their_message, parent, false));
			this.message = (TextView) this.itemView.findViewById(R.id.message);
			this.age = (TimestampView) this.itemView.findViewById(R.id.age);
		}

		public void bind(MeshMSMessage item, MeshMSMessage previous) {

//			Log.e("San","iid-  "+item.getId()+" text- "+item.text+"");
//			Log.e("San","lId-  "+lastId+" text- "+item.text+"");
//			Log.e("San","text-  "+item.text+"");
			if(isLastMsg)
			{
//				Log.e("San","item_id...-  "+item.getId()+"");
//				Log.e("San","text...-  "+item.text+"");

					isLastMsg = false;
//				lastId =item.getId();
				prefPeerLastMsg = item.text;

				MainActivity.editor = MainActivity.pref.edit();
				MainActivity.editor.putString(prefPeerId, prefPeerLastMsg+"_date_"+item.date);
				MainActivity.editor.commit();
			}

			Log.d("TextItemHolder", String.format("bind: %s", item.text));

			message.setText(item.text);
			age.setDates(item.date, previous==null?null:previous.date);
		}
	}

	private class AckHolder extends ItemHolder{
		public AckHolder(LayoutInflater inflater, ViewGroup parent) {
			super(inflater.inflate(R.layout.ack_message, parent, false));
		}

		@Override
		public void bind(MeshMSMessage item, MeshMSMessage previous) {
		}
	}
}
