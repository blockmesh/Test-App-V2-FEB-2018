package org.servalproject.servalchat.peer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.servalproject.mid.KnownPeers;
import org.servalproject.mid.Peer;
import org.servalproject.servalchat.R;
import org.servalproject.servalchat.navigation.Events;
import org.servalproject.servalchat.navigation.MainActivity;
import org.servalproject.servalchat.navigation.Navigation;
import org.servalproject.servalchat.views.Identicon;
import org.servalproject.servaldna.SigningKey;
import org.servalproject.servaldna.Subscriber;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jeremy on 30/05/17.
 */
public class PeerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
	private final ImageView icon;
	private final TextView name;
	private final MainActivity activity;
	private Subscriber subscriber;

	private Peer peer;
	Map<String, String> map = new HashMap<String, String>();
	public PeerHolder(MainActivity activity, ViewGroup parent) {
		super(LayoutInflater.from(parent.getContext()).inflate(R.layout.peer, parent, false));
		this.activity = activity;
		//avatar = (ImageView)this.itemView.findViewById(R.id.avatar);
		name = (TextView) this.itemView.findViewById(R.id.name);
		icon = (ImageView) this.itemView.findViewById(R.id.identicon);
		this.itemView.setOnClickListener(this);

		map.put("a", "#EF5350");
		map.put("b", "#EC407A");
		map.put("c", "#AB47BC");
		map.put("d", "#7E57C2");
		map.put("e", "#5C6BC0");
		map.put("f", "#42A5F5");
		map.put("g", "#29B6F6");
		map.put("h", "#26C6DA");
		map.put("i", "#26A69A");
		map.put("j", "#66BB6A");
		map.put("k", "#9CCC65");
		map.put("l", "#D4E157");
		map.put("m", "#FFEE58");
		map.put("n", "#FFCA28");
		map.put("o", "#FFA726");
		map.put("p", "#FF7043");
		map.put("q", "#8D6E63");
		map.put("r", "#EF5350");
		map.put("s", "#EC407A");
		map.put("t", "#AB47BC");
		map.put("u", "#7E57C2");
		map.put("v", "#5C6BC0");
		map.put("w", "#42A5F5");
		map.put("x", "#29B6F6");
		map.put("y", "#26C6DA");
		map.put("z", "#26A69A");
		map.put("0", "#66BB6A");
		map.put("1", "#9CCC65");
		map.put("2", "#D4E157");
		map.put("3", "#FFEE58");
		map.put("4", "#FFCA28");
		map.put("5", "#FFA726");
		map.put("6", "#FF7043");
		map.put("7", "#8D6E63");
		map.put("8", "#D6E455");
		map.put("9", "#8D6489");
		map.put("#", "#7D6489");
	}

	public void bind(Peer peer) {
		this.peer = peer;
		this.name.setText(peer.displayName());
		this.name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
//		SigningKey key = peer.getSubscriber().signingKey;
//		if (key == null){
//			this.icon.setVisibility(View.INVISIBLE);
//		}else{
//			this.icon.setImageDrawable(new Identicon(key));
//			this.icon.setVisibility(View.VISIBLE);
//		}
		this.icon.setImageBitmap(retrieveContactPhoto(peer.displayName().subSequence(0, 1).toString().toUpperCase()));

		this.name.setOnClickListener(this);

		if (peer.isReachable()) {
			// Show green dot?
		}
	}

	@Override
	public void onClick(View v) {
//		activity.go(Navigation.PeerDetails, peer, null);
		//activity.go(Navigation.PrivateMessages, peer, null);
		EventBus.getDefault().post(new Events.ChatSelected(peer));
	}

	private Bitmap retrieveContactPhoto(String text) {
		Bitmap photo = null;
		if (text.equals("+")) {
			text = "#";
		} else if (text.equals("0")) {
			text = "#";
		} else if (text.equals("1")) {
			text = "#";
		} else if (text.equals("2")) {
			text = "#";
		} else if (text.equals("3")) {
			text = "#";
		} else if (text.equals("4")) {
			text = "#";
		} else if (text.equals("5")) {
			text = "#";
		} else if (text.equals("6")) {
			text = "#";
		} else if (text.equals("7")) {
			text = "#";
		} else if (text.equals("8")) {
			text = "#";
		} else if (text.equals("9")) {
			text = "#";
		} else {
			text = text;
		}
//			String[] c_code = new String[]
//					{
//
//					};


//			int random = (int) Math.ceil(Math.random() * 10);


		ShapeDrawable Circle = new ShapeDrawable(new OvalShape());
		Drawable[] d = {Circle};
		Drawable c1 = new LayerDrawable(d);

//			Bitmap icon = BitmapFactory.decodeResource(getActivity()
//					.getResources(), R.mipmap.ic_launcher);
//		Bitmap src = BitmapFactory.decodeResource(MainActivity.contextMain.getResources(), R.mipmap.ic_launcher); // the original file
		// yourimage.jpg i added
		// in resources
		Bitmap dest = Bitmap.createBitmap(80,
				80, Bitmap.Config.ARGB_8888);
		// String yourText = "My custom Text adding to Image";

		Canvas cs = new Canvas(dest);

		Paint p = new Paint();
		p.setAntiAlias(true);
		//p.setColor(Color.parseColor(c_code[i1]));
		if (!text.equalsIgnoreCase("#"))
		{
			p.setColor(Color.parseColor(map.get(text.toLowerCase())));
		} else {
			p.setColor(Color.parseColor(map.get(text)));

		}
		p.setStyle(Paint.Style.FILL);

		Paint tPaint = new Paint();
		tPaint.setTextSize(40);
		tPaint.setColor(Color.WHITE);
		tPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		tPaint.setFakeBoldText(true);
		tPaint.setTextAlign(Paint.Align.CENTER);
		//cs.drawBitmap(src, 0f, 0f, null);
		cs.drawCircle(40, 40, 35, p);
		float height = tPaint.measureText("yY");
		float width = tPaint.measureText(text);
//		float x_coord = (src.getWidth() - width) / 2;

		int xPos = (cs.getWidth() / 2);
		int yPos = (int) ((cs.getHeight() / 2) - ((tPaint.descent() + tPaint
				.ascent()) / 2));

		// cs.drawText(text, x_coord, height, tPaint);
		cs.drawText(text, xPos, yPos, tPaint);// 15f is to put space between
		// top edge and the text, if
		// you want to change it,


		photo = dest;
		return photo;
	}
}
