package org.servalproject.servalchat.feeds;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import org.servalproject.mid.Identity;
import org.servalproject.mid.Peer;
import org.servalproject.servalchat.R;
import org.servalproject.servalchat.navigation.ILifecycle;
import org.servalproject.servalchat.navigation.INavigate;
import org.servalproject.servalchat.navigation.MainActivity;
import org.servalproject.servalchat.navigation.Navigation;
import org.servalproject.servalchat.views.BottomNavigationListener;
import org.servalproject.servalchat.views.BottomNavigationViewHelper;
import org.servalproject.servalchat.views.RecyclerHelper;

/**
 * Created by jeremy on 11/10/16.
 */
//public class PublicFeedsList extends RecyclerView implements INavigate {
//	PublicFeedsPresenter presenter;
//	MainActivity activity;
//
//	public PublicFeedsList(Context context, @Nullable AttributeSet attrs) {
//		super(context, attrs);
//	}
//
//	@Override
//	public ILifecycle onAttach(MainActivity activity, Navigation n, Identity id, Peer peer, Bundle args) {
//		this.activity = activity;
//		RecyclerHelper.createLayoutManager(this, true, false);
//		RecyclerHelper.createDivider(this);
//		presenter = PublicFeedsPresenter.factory.getPresenter(this, id, peer, args);
//		return presenter;
//	}
//}
public class PublicFeedsList extends CoordinatorLayout implements INavigate {
	private static final String TAG = PublicFeedsList.class.getSimpleName();

	public PublicFeedsPresenter presenter;
	MainActivity activity;
	RecyclerView list;
	FloatingActionButton flotNewChat;
	BottomNavigationView bottomNavigationView;
	public PublicFeedsList(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public ILifecycle onAttach(MainActivity activity, Navigation n, Identity id, Peer peer, Bundle args) {
		Log.d(TAG, "onAttach");

		this.activity = activity;
		this.list = (RecyclerView) findViewById(R.id.list);
		RecyclerHelper.createLayoutManager(list, true, false);
		RecyclerHelper.createDivider(list);

		presenter = PublicFeedsPresenter.factory.getPresenter(this, id, peer, args);
		this.flotNewChat = (FloatingActionButton)findViewById(R.id.fab_chat);

		final MainActivity activity1 =    activity ;
		this.flotNewChat.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				activity1.go(Navigation.NewMessageList);
			}
		});

		activity1.addReachablesPeers();

		return presenter;
	}
}
