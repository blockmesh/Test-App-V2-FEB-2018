package org.servalproject.servalchat.feeds;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.servalproject.Extras;
import org.servalproject.mid.FeedList;
import org.servalproject.mid.Identity;
import org.servalproject.mid.KnownPeers;
import org.servalproject.mid.MessageList;
import org.servalproject.mid.Peer;
import org.servalproject.mid.Serval;
import org.servalproject.servalchat.App;
import org.servalproject.servalchat.navigation.Events;
import org.servalproject.servalchat.navigation.MainActivity;
import org.servalproject.servalchat.navigation.Navigation;
import org.servalproject.servalchat.views.Presenter;
import org.servalproject.servalchat.views.PresenterFactory;
import org.servalproject.servaldna.Subscriber;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by jeremy on 11/10/16.
 */
public class PublicFeedsPresenter extends Presenter<PublicFeedsList> {
	private static final String TAG = PublicFeedsPresenter.class.getSimpleName();
	public static FeedListAdapter adapter;
	private MessageList messages;
	PublicFeedsList view;
	FeedList feedList;
	private MediaPlayer mp = new MediaPlayer();
	protected PublicFeedsPresenter(PresenterFactory<PublicFeedsList, ?> factory, String key, Identity identity) {
		super(factory, key, identity);
	}

	public static PresenterFactory<PublicFeedsList, PublicFeedsPresenter> factory
			= new PresenterFactory<PublicFeedsList, PublicFeedsPresenter>() {

		@Override
		protected PublicFeedsPresenter create(String key, Identity id, Peer peer) {
			return new PublicFeedsPresenter(this, key, id);
		}
	};

	@Override
	protected void bind(PublicFeedsList view1) {
		MainActivity.contextMain.registerReceiver(form_filled, new IntentFilter("blockMesh_send_brodcast"));
		this.view = view1;
//		this.view.setAdapter(adapter);
		this.view.list.setAdapter(adapter);
		Log.d(TAG, String.format("bind: %d list hash code: %d", hashCode(), view.list.hashCode()));
	}

	@Override
	protected void restore(Bundle config) {
		Log.d(TAG, "restore");
		feedList = identity.getAllFeeds();
		adapter = new FeedListAdapter(feedList, this);
	}

	public void openFeed(Subscriber subscriber) {
		PublicFeedsList list = getView();
		if (list == null)
			return;
		if (identity.subscriber.equals(subscriber)) {
			Log.d(TAG, "openFeed: myself");
			//list.activity.go(Navigation.MyFeed);
		}
		else {
			//Log.e("San", "PrivateMessages");
			Peer peer = Serval.getInstance().knownPeers.getPeer(subscriber);
			EventBus.getDefault().post(new Events.ChatSelected(peer));
			//list.activity.go(Navigation.PrivateMessages, peer, null);
		}
	}

	BroadcastReceiver form_filled = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String received_action = intent.getAction();

			if (received_action.equals("blockMesh_send_brodcast")) {
				ArrayList<String> peerIds = intent.getStringArrayListExtra(Extras.PEER_IDS);
				String peerId = intent.getStringExtra(Extras.PEER_ID);

				Log.e(TAG,String.format("Broadcast Received successfully Peer Ids: %s Peer Id: %s My identity: %s", peerIds, peerId, identity.subscriber.sid.toString()));
				feedList = MainActivity.identity.getAllFeeds();

				MainActivity activity = getActivity();
				if (activity != null) {
					for (String id : peerIds) {
						activity.addPeerAsChatter(id);
					}
					//activity.addPeerAsChatter(peerId);
				}

//				adapter = new FeedListAdapter(feedList, PublicFeedsPresenter.this);
//				view.setAdapter(adapter);
				adapter.notifyDataSetChanged();
//				Log.e("San","isMsgReciever "+App.isMsgReciever);
//				if(App.isMsgReciever) {
//					App.isMsgReciever = false;
//
//					Log.e("San","isMsgSender "+App.isMsgReciever);
					if (mp.isPlaying()) {
						mp.stop();
					}
					try {
						mp.reset();
						AssetFileDescriptor afd;
//					Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//					builder.setSound(uri);
						afd = MainActivity.contextMain.getAssets().openFd("sound.mpeg");
						mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
						mp.prepare();
						mp.start();
					} catch (IllegalStateException e) {
//					e.printStackTrace();
						Log.e("San", " " + e);
					} catch (IOException e) {
//					e.printStackTrace();
						Log.e("San", " " + e);
					}
//				}

			}
		}
	};

	protected MainActivity getActivity() {
		PublicFeedsList view = getView();
		return view == null ? null : view.activity;
	}

	@Override
	public void onVisible() {
		Log.d(TAG, String.format("onVisible: %d", hashCode()));
		super.onVisible();
		adapter.onVisible();
	}

	@Override
	public void onHidden() {
		Log.d(TAG, String.format("onHidden: %d", hashCode()));
		super.onHidden();
		adapter.onHidden();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		//adapter.clear2();
		//view.list.setAdapter(null);
		//adapter = null;
	}
}
