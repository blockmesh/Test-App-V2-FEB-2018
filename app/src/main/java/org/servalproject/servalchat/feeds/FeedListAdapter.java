package org.servalproject.servalchat.feeds;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.servalproject.mid.FeedList;
import org.servalproject.mid.IObservableList;
import org.servalproject.mid.MessageList;
import org.servalproject.mid.Messaging;
import org.servalproject.mid.Serval;
import org.servalproject.servalchat.R;
import org.servalproject.servalchat.navigation.Events;
import org.servalproject.servalchat.navigation.MainActivity;
import org.servalproject.servalchat.peer.PrivateMessaging;
import org.servalproject.servalchat.peer.PrivateMessagingPresenter;
import org.servalproject.servalchat.views.BasicViewHolder;
import org.servalproject.servalchat.views.Identicon;
import org.servalproject.servalchat.views.ScrollingAdapter;
import org.servalproject.servaldna.BundleId;
import org.servalproject.servaldna.ServalDInterfaceException;
import org.servalproject.servaldna.Subscriber;
import org.servalproject.servaldna.meshms.MeshMSException;
import org.servalproject.servaldna.meshms.MeshMSMessage;
import org.servalproject.servaldna.meshms.MeshMSMessageList;
import org.servalproject.servaldna.rhizome.RhizomeListBundle;

import java.io.IOException;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import agency.tango.android.avatarview.utils.StringUtils;

/**
 * Created by jeremy on 11/10/16.
 */
public class FeedListAdapter extends ScrollingAdapter<RhizomeListBundle, FeedListAdapter.FeedHolder> {
	private static final String TAG = FeedListAdapter.class.getSimpleName();

	private MessageList messages;
	private final PublicFeedsPresenter presenter;
	private HashSet<BundleId> bundles = new HashSet<>();
	boolean isNewPeerUser = false;
	Map<String, String> map = new HashMap<String, String>();
	private List<String> tmpList = new ArrayList<>();
	private String myIdentity;

	public FeedListAdapter(FeedList list, PublicFeedsPresenter presenter) {
		super(list);
		this.presenter = presenter;
		map.put("a", "#EF5350");
		map.put("b", "#EC407A");
		map.put("c", "#AB47BC");
		map.put("d", "#7E57C2");
		map.put("e", "#5C6BC0");
		map.put("f", "#42A5F5");
		map.put("g", "#29B6F6");
		map.put("h", "#26C6DA");
		map.put("i", "#26A69A");
		map.put("j", "#66BB6A");
		map.put("k", "#9CCC65");
		map.put("l", "#D4E157");
		map.put("m", "#FFEE58");
		map.put("n", "#FFCA28");
		map.put("o", "#FFA726");
		map.put("p", "#FF7043");
		map.put("q", "#8D6E63");
		map.put("r", "#EF5350");
		map.put("s", "#EC407A");
		map.put("t", "#AB47BC");
		map.put("u", "#7E57C2");
		map.put("v", "#5C6BC0");
		map.put("w", "#42A5F5");
		map.put("x", "#29B6F6");
		map.put("y", "#26C6DA");
		map.put("z", "#26A69A");
		map.put("0", "#66BB6A");
		map.put("1", "#9CCC65");
		map.put("2", "#D4E157");
		map.put("3", "#FFEE58");
		map.put("4", "#FFCA28");
		map.put("5", "#FFA726");
		map.put("6", "#FF7043");
		map.put("7", "#8D6E63");
		map.put("8", "#D6E455");
		map.put("9", "#8D6489");
		map.put("#", "#7D6489");

		myIdentity = Serval.getInstance().identities.getIdentities().get(0).subscriber.toString();

	}

	private void removeItem(BundleId id) {
		// find the old item and remove it.
		for (int i = 0; i < items.size(); i++) {

			if (items.get(i).manifest.id.equals(id)) {
				items.remove(i);
				return;
			}
		}
	}

	@Override
	protected void addItem(int index, RhizomeListBundle item) {
		String token = item.author.abbreviation();
		if (tmpList.contains(token)) {
			return;
		}

		tmpList.add(token);
		boolean isChatter = false;
		MainActivity mainActivity = getActivity();

		if (mainActivity != null) {
			//mainActivity.addReachablesPeers();
			isChatter = mainActivity.isChatter(item);
		}

//		Subscriber subscriber = new Subscriber(
//				item.author != null ? item.author : item.manifest.sender,
//				item.manifest.id, true);

//		Log.e("San","sender:- "+item.manifest.sender+" --- "+item.author+" --- "+item.manifest.id+" "+item.manifest.name);
//		BundleId id = item.manifest.id;
//		Messaging.SubscriptionState state = presenter.identity.messaging.getSubscriptionState(subscriber);
//		if (state == Messaging.SubscriptionState.Blocked)
//			return;
//		if (bundles.contains(id)) {
//			if (index == items.size())
//				return;
//			removeItem(id);
//		}else{
//			bundles.add(id);
//		}

		// KINE: commented because it's strange

		/*
		isNewPeerUser = false;
		for(int k=0; k<MainActivity.mLivePeerList.size();k++ ) {
			if(MainActivity.mLivePeerList.get(k).equals(item.author.toString() ))
			{
				isNewPeerUser = true;
			}
		}
		*/

		//Log.d(TAG, String.format("addItem: %s %b %b %b", item.author.toString(), MainActivity.pref.getString(item.author.toString(), null) != null, isNewPeerUser, isChatter));
		Log.d(TAG, String.format("addItem: %s isChatter: %b token: %s", item.author.toString(), isChatter, item.token));

		if (items != null) {
			for (RhizomeListBundle obj : items) {
				Log.d(TAG, String.format("addItem: existent item %s new item %s", obj.author.abbreviation(), item.author.abbreviation()));
				if (obj.author.abbreviation().equals(item.author.abbreviation())) {
					int index2 = items.indexOf(obj);
					Log.d(TAG, String.format("addItem: already included. Indexes: %d %d", index, index2));

					try {
						items.set(index2, item);
						notifyItemChanged(index2);
					} catch (Exception e) {
						e.printStackTrace();
					}

					return;
				}
			}
		}

		//EventBus.getDefault().post(new Events.FeedAdded(item, isChatter));

		if (!item.author.toString().equals(myIdentity)) {
			EventBus.getDefault().post(new Events.FeedAdded(item, true));
			super.addItem(index, item);
		}

		/*
		//if((MainActivity.pref.getString(item.author.toString(), null) != null) || isNewPeerUser || isChatter) {
		if (isChatter) {
			try {
				super.addItem(index, item);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		*/

		tmpList.remove(token);
	}

	@Override
	protected MainActivity getActivity() {
		return presenter.getActivity();
	}

	@RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void bind(FeedHolder holder, RhizomeListBundle item) {
		holder.bind(item);
	}

	@Override
	public FeedHolder create(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		return new FeedHolder(inflater.inflate(R.layout.feed, parent, false));
	}

	public class FeedHolder extends BasicViewHolder implements View.OnClickListener{
		private TextView name,txtMessage,txtDate;
		private ImageView icon;
		private Subscriber subscriber;
		private View viewOnOff;
		private LinearLayout llItemMain;

		public FeedHolder(View itemView) {
			super(itemView);
//			this.llItemMain = (LinearLayout) this.itemView.findViewById(R.id.ll_item_main);
			this.name = (TextView) this.itemView.findViewById(R.id.name);
			this.txtMessage = (TextView) this.itemView.findViewById(R.id.txt_message);
			this.txtDate = (TextView) this.itemView.findViewById(R.id.txt_date);
			this.icon = (ImageView) this.itemView.findViewById(R.id.identicon);
			this.viewOnOff = (View) this.itemView.findViewById(R.id.view_on_off);
			this.itemView.setOnClickListener(this);
		}

		@RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
		public void bind(RhizomeListBundle item) {

			subscriber = new Subscriber(
					item.author != null ? item.author : item.manifest.sender,
					item.manifest.id, true);
//			Log.e("San","subscriber: " + item.author+" "+item.manifest.sender+" "+item.manifest.id+item.manifest.name);
//			this.icon.setImageDrawable(new Identicon(item.manifest.id));
			messages = MainActivity.identity.messaging.getPrivateMessages(subscriber);


			viewOnOff.setBackgroundResource(R.drawable.circle_fill_gray);
			for(int k=0; k<MainActivity.mLivePeerList.size();k++ ) {
				if(MainActivity.mLivePeerList.get(k).equals(item.author.toString() ))
				{
					viewOnOff.setBackgroundResource(R.drawable.circle_fill_green);
				}
			}

			if (item.manifest.name == null || "".equals(item.manifest.name))
				name.setText(subscriber.sid.abbreviation());
			else
				name.setText(item.manifest.name);

			name.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					presenter.openFeed(subscriber);
				}
			});

//			name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

			this.icon.setImageBitmap(retrieveContactPhoto(item.manifest.name.subSequence(0, 1).toString().toUpperCase()));

//			String textMsg  = MainActivity.pref.getString(messages.peer.sid.toString(), null) !=null ? MainActivity.pref.getString(messages.peer.sid.toString(), null): null ;

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);

			txtMessage.setText("");
			txtDate.setText("");

			try {
//				MeshMSMessageList meshMSMessageList = MainActivity.serval.getResultClient().meshmsListMessages(messages.self.sid, messages.peer.sid);
//				int msgCount = meshMSMessageList.toList().size();
//				int msgCount = 1;
//				Log.e("San","msgCount: " + msgCount);

//				if (msgCount > 0) {
					IObservableList list = messages;
					MeshMSMessage msg = null;
//
//					for(int i =0;i<msgCount;i++){
//						msg = (MeshMSMessage) list.next();
//						Log.e("San","Alisha msg: " + msg.text);
//					}
				if(list != null) {
					for(int i =0;i<2;i++) {
//												Log.e("San","i: " + i);

						if ((msg = (MeshMSMessage) list.next()) != null) {

							if(msg.text != null) {
//								Log.e("San","isDelivered:- "+msg.isDelivered+" isRead:- "+msg.isRead+" - "+msg.type+" - "+msg.me+" - "+msg.theirOffset+" - "+msg.myOffset);

								if((!msg.isRead ) && (msg.type.toString().equals("MESSAGE_RECEIVED")))
									txtMessage.setAlpha(1.0f);

									txtMessage.setText(msg.text);
								txtDate.setText(parseDateToddMMyyyy(msg.date + ""));
								break;
							}
						}
					}
				}
//				}
			} catch (ServalDInterfaceException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (MeshMSException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}


		}

			@Override
		public void onClick(View v) {
			presenter.openFeed(subscriber);
		}



	}


	public String parseDateToddMMyyyy(String time) {
		String inputPattern = "EEE MMM dd HH:mm:ss zzz yyyy";
		SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);

		Date date = null;
		String str = null;

		try {
			date = inputFormat.parse(time);
			Calendar now = Calendar.getInstance();
			Calendar smsTime = Calendar.getInstance();

			smsTime.setTimeInMillis(date.getTime());

			if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
				Log.d(TAG, "parseDateToddMMyyyy: case1");
				String outputPatternToday = "hh:mm aa";
				SimpleDateFormat outputFormatToday = new SimpleDateFormat(outputPatternToday, Locale.US);
				str = outputFormatToday.format(date);
			} else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
				Log.d(TAG, "parseDateToddMMyyyy: case2");
				str = "Yesterday";
			} else {
				Log.d(TAG, "parseDateToddMMyyyy: case3");
				String outputPattern = "dd MMM";
				SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);
				str = outputFormat.format(date);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return str;
	}

	private Bitmap retrieveContactPhoto( String text) {
		Bitmap photo = null;
		if (text.equals("+")) {
			text = "#";
		} else if (text.equals("0")) {
			text = "#";
		} else if (text.equals("1")) {
			text = "#";
		} else if (text.equals("2")) {
			text = "#";
		} else if (text.equals("3")) {
			text = "#";
		} else if (text.equals("4")) {
			text = "#";
		} else if (text.equals("5")) {
			text = "#";
		} else if (text.equals("6")) {
			text = "#";
		} else if (text.equals("7")) {
			text = "#";
		} else if (text.equals("8")) {
			text = "#";
		} else if (text.equals("9")) {
			text = "#";
		} else {
			text = text;
		}
//			String[] c_code = new String[]
//					{
//
//					};


//			int random = (int) Math.ceil(Math.random() * 10);


		ShapeDrawable Circle = new ShapeDrawable(new OvalShape());
		Drawable[] d = {Circle};
		Drawable c1 = new LayerDrawable(d);

//			Bitmap icon = BitmapFactory.decodeResource(getActivity()
//					.getResources(), R.mipmap.ic_launcher);
		Bitmap src = BitmapFactory.decodeResource(getActivity()
				.getResources(), R.mipmap.ic_launcher); // the original file
		// yourimage.jpg i added
		// in resources
		Bitmap dest = Bitmap.createBitmap(80,
				80, Bitmap.Config.ARGB_8888);
		// String yourText = "My custom Text adding to Image";

		Canvas cs = new Canvas(dest);

		Paint p = new Paint();
		p.setAntiAlias(true);
		//p.setColor(Color.parseColor(c_code[i1]));
		if (!text.equalsIgnoreCase("#"))
		{
			p.setColor(Color.parseColor(map.get(text.toLowerCase())));
	} else {
			p.setColor(Color.parseColor(map.get(text)));

		}
			p.setStyle(Paint.Style.FILL);

			Paint tPaint = new Paint();
			tPaint.setTextSize(40);
			tPaint.setColor(Color.WHITE);
			tPaint.setStyle(Paint.Style.FILL_AND_STROKE);
			tPaint.setFakeBoldText(true);
			tPaint.setTextAlign(Paint.Align.CENTER);
			//cs.drawBitmap(src, 0f, 0f, null);
			cs.drawCircle(40, 40, 35, p);
			float height = tPaint.measureText("yY");
			float width = tPaint.measureText(text);
			float x_coord = (src.getWidth() - width) / 2;

			int xPos = (cs.getWidth() / 2);
			int yPos = (int) ((cs.getHeight() / 2) - ((tPaint.descent() + tPaint
					.ascent()) / 2));

			// cs.drawText(text, x_coord, height, tPaint);
			cs.drawText(text, xPos, yPos, tPaint);// 15f is to put space between
			// top edge and the text, if
			// you want to change it,


			photo = dest;
		return photo;
	}
}
