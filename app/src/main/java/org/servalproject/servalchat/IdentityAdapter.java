package org.servalproject.servalchat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.servalproject.mid.FeedList;
import org.servalproject.servalchat.navigation.MainActivity;
import org.servalproject.servalchat.views.BasicViewHolder;
import org.servalproject.servalchat.views.Identicon;
import org.servalproject.servalchat.views.ScrollingAdapter;
import org.servalproject.servaldna.BundleId;
import org.servalproject.servaldna.Subscriber;
import org.servalproject.servaldna.rhizome.RhizomeListBundle;

import java.util.HashSet;

public class IdentityAdapter extends MessageScrollingAdapter<RhizomeListBundle,IdentityAdapter.FeedHolder> {
    private HashSet<BundleId> bundles = new HashSet<>();
    private MessagesActivity messagesActivity;

    public IdentityAdapter(FeedList list, MessagesActivity messagesActivity1) {
        super(list);
        this.messagesActivity = messagesActivity1;
//        Log.e("San" ,"list: "+list );
    }

    private void removeItem(BundleId id) {
        // find the old item and remove it.
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).manifest.id.equals(id)) {
                items.remove(i);
                return;
            }
        }
    }

    @Override
    protected void addItem(int index, RhizomeListBundle item) {
        Subscriber subscriber = new Subscriber(
                item.author != null ? item.author : item.manifest.sender,
                item.manifest.id, true);
        BundleId id = item.manifest.id;
//        Messaging.SubscriptionState state = presenter.identity.messaging.getSubscriptionState(subscriber);
//        if (state == Messaging.SubscriptionState.Blocked)
//            return;

        if (bundles.contains(id)) {
            if (index == items.size())
                return;
            removeItem(id);
        }else{
            bundles.add(id);
        }
//        Log.e("San" ,"INDEX "+index +" I "+item );
        super.addItem(index, item);
    }

    @Override
    protected MessagesActivity getActivity() {
        return messagesActivity;
    }

    @Override
    protected void bind(FeedHolder holder, RhizomeListBundle item) {
        holder.bind(item);
    }

    @Override
    public FeedHolder create(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        return new IdentityAdapter.FeedHolder(inflater.inflate(R.layout.feed, parent, false));
    }

    public class FeedHolder extends BasicViewHolder implements View.OnClickListener {
        private TextView name;
        private ImageView icon;
        private Subscriber subscriber;

        public FeedHolder(View itemView) {
            super(itemView);
            this.name = (TextView) this.itemView.findViewById(R.id.name);
            this.icon = (ImageView) this.itemView.findViewById(R.id.identicon);
            this.itemView.setOnClickListener(this);
        }

        public void bind(RhizomeListBundle item) {
            subscriber = new Subscriber(
                    item.author != null ? item.author : item.manifest.sender,
                    item.manifest.id, true);
            this.icon.setImageDrawable(new Identicon(item.manifest.id));
            if (item.manifest.name == null || "".equals(item.manifest.name))
                name.setText(subscriber.sid.abbreviation());
            else
                name.setText(item.manifest.name);

//            Log.e("San" ,"manifest "+item.manifest.name );
        }

        @Override
        public void onClick(View v) {
//            presenter.openFeed(subscriber);
        }
    }
}

