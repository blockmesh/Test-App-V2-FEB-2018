package org.servalproject.servalchat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.servalproject.servalchat.navigation.Events;
import org.servalproject.servalchat.navigation.MainActivity;
import org.servalproject.servalchat.views.BottomNavigationListener;
import org.servalproject.servalchat.views.BottomNavigationViewHelper;

import java.util.ArrayList;

/**
 * Created by falcon on 13/12/17.
 */

public class ContactActivity extends AppCompatActivity  {
    ImageView imgBack,imgSearch,imgClose;
    TextView txtTitle;
    EditText etSearch;
    LinearLayout llTop;
    ListView listContacts;
    private ArrayList<ContactItemList> contactItemLists = new ArrayList<ContactItemList>();
    private ArrayList<ContactItemList> contactItemLists1 = new ArrayList<ContactItemList>();
    private ArrayList<ContactItemList> contactItemLists2 = new ArrayList<ContactItemList>();
    private ContactAdapter contactAdapter;
    CharSequence filterString="";
    Cursor cursor ;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_2);

        progressDialog = new ProgressDialog(ContactActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        double ww = MainActivity.width/3;
        double hh = MainActivity.height/5;

        imgClose = (ImageView) findViewById(R.id.img_close);
        imgBack = (ImageView) findViewById(R.id.img_more);
        imgSearch = (ImageView) findViewById(R.id.img_search);
        txtTitle = (TextView)findViewById(R.id.txt_title);
        etSearch = (EditText)findViewById(R.id.et_search);
        llTop = (LinearLayout)findViewById(R.id.ll_top);
        listContacts = (ListView) findViewById(R.id.list_contacts);

        // Commented by kine
        //LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,((int)hh/7)*3);
        //llTop.setLayoutParams(parms);

        //BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        //BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);
        //bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationListener(this, true, R.id.navigation_contacts));

        RelativeLayout.LayoutParams parmsImgMore = new RelativeLayout.LayoutParams((int)hh/3,(int)hh/3);
        imgBack.setPadding((int)hh/13,0,0,0);
        parmsImgMore.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        parmsImgMore.addRule(RelativeLayout.CENTER_VERTICAL);
        imgBack.setLayoutParams(parmsImgMore);

        RelativeLayout.LayoutParams parmsImgSearch = new RelativeLayout.LayoutParams((int)hh/3,(int)hh/3);
        parmsImgSearch.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        parmsImgSearch.addRule(RelativeLayout.CENTER_VERTICAL);
        imgSearch.setPadding((int)hh/13,0,0,0);
        imgSearch.setLayoutParams(parmsImgSearch);

        RelativeLayout.LayoutParams parmsImgClose = new RelativeLayout.LayoutParams((int)hh/3,(int)hh/3);
        imgClose.setPadding((int)hh/13,0,0,0);
        parmsImgClose.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        parmsImgClose.addRule(RelativeLayout.CENTER_VERTICAL);
        imgClose.setLayoutParams(parmsImgClose);
        imgClose.setVisibility(View.GONE);

        txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)hh/5);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactActivity.this.finish();

            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgClose.setVisibility(View.VISIBLE);
                etSearch.setVisibility(View.VISIBLE);
                etSearch.setFocusable(true);
                etSearch.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
                txtTitle.setVisibility(View.GONE);
                imgSearch.setVisibility(View.GONE);

            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgClose.setVisibility(View.GONE);
                etSearch.setVisibility(View.GONE);
                txtTitle.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.VISIBLE);
                etSearch.setText("");
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);

            }
        });

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getContact();
            }
        });
        thread.start();


        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                filterString = cs;
                contactItemLists.addAll(contactItemLists2);
                Log.e("San ",arg1+" "+arg2+" "+arg3);
               getFilter();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        listContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String number = contactItemLists.get(position).getPhNumber();

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+number));
                try {
                    startActivity(callIntent);
                }
                catch (SecurityException se)
                {

                }catch (Exception e){}
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public void getContact() {
        cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        while (cursor.moveToNext()) {
//
            ContactItemList contactItemList = new ContactItemList();

            contactItemList.setName( cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));

            contactItemList.setPhNumber(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));

            if (!contactItemLists.contains(contactItemList))
                contactItemLists.add(contactItemList);
//            StoreContacts.add(name + " "  + ":" + " " + phonenumber);
        }

        cursor.close();

        ContactActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                contactItemLists2.addAll(contactItemLists);
                contactAdapter  = new ContactAdapter(getApplicationContext(),contactItemLists,false);
                contactAdapter.notifyDataSetChanged();
                listContacts.setAdapter(contactAdapter);
            }
        });



        try{
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }catch(Exception exception){
            exception.printStackTrace();
        }

    }


//
//    @Override
//    public Filter getFilter() {
//        // TODO Auto-generated method stub
//        Filter filter = new Filter() {
//
//            @Override
//            protected void publishResults(CharSequence constraint,
//                                          FilterResults results) {
//                // TODO Auto-generated method stub
//                arr = (ArrayList<ContactItemList>) results.values;
//                notifyDataSetChanged();
//            }
//
//            @Override
//            protected FilterResults performFiltering(CharSequence constraint) {
//                // TODO Auto-generated method stub
//                FilterResults results = new FilterResults();
//
//                if (constraint == null || constraint.length() == 0) {
//                    // No filter implemented we return all the list
//                    results.values = contactItemLists;
//                    results.count = contactItemLists.size();
//
//                    isFilter = false;
//
//                } else {
//
//                    FilteredList.clear();
//                    isFilter = true;
//                    Log.e("San"," name");
//                    for (int i = 0; i < contactItemLists.size(); i++) {
//
//                        ContactItemList data1 = contactItemLists.get(i);
//
//
//                        if (data1.name.toString().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
//                            FilteredList.add(data1);
//
//                            Log.e("San",data1.name.toString()+" name");
//                        }
//
//                    }
//
//                    results.values = FilteredList;
//                    results.count = FilteredList.size();
//
//                }
//                return results;
//            }
//        };
//        return filter;
//    }


    public void  getFilter()
    {
        contactItemLists1.clear();
        contactItemLists1.addAll(contactItemLists);

        contactItemLists.clear();
        if(filterString.toString().length() >0 ) {
            for (int i = 0; i < contactItemLists1.size(); i++) {

                ContactItemList data1 = contactItemLists1.get(i);

//            Log.e("San","contactItemLists1:- "+contactItemLists1.size());
                if (data1.name.toString().toLowerCase().contains(filterString.toString().toLowerCase())) {
//            if (data1.name.toString().startsWith(filterString.toString())) {
                    contactItemLists.add(data1);
//                Log.e("San",data1.name.toString()+" name");
                }

            }
        }
        else
        {
            contactItemLists.addAll(contactItemLists2);
        }
//        Log.e("San","size"+contactItemLists.size());
        contactAdapter.notifyDataSetChanged();
//        listContacts.setAdapter(contactAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        EventBus.getDefault().post(new Events.FinishMainActivity());
    }
}