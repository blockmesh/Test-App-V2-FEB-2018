package org.servalproject.servalchat.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import org.servalproject.servalchat.R;

/**
 * Created by rodrigobrauwers on 14/02/18.
 */

public class CustomToolbar extends Toolbar {
    private static String TAG = CustomToolbar.class.getSimpleName();
    private TextView customTitleView;

    public CustomToolbar(Context context) {
        super(context);
        init();
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        init();
    }

    private void init() {
        customTitleView = findViewById(R.id.customTitleView);
    }

    @Override
    public void setTitle(int resId) {
        if (customTitleView != null) {
            customTitleView.setText(resId);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        Log.d(TAG, String.format("setTitle: %s customTitleView is null: %b", title, customTitleView == null));
        if (customTitleView != null) {
            customTitleView.setText(title);
        }
    }
}

