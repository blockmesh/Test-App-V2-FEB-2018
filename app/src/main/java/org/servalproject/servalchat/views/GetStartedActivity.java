package org.servalproject.servalchat.views;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.servalproject.SharedPrefsKeys;
import org.servalproject.mid.Serval;
import org.servalproject.mid.networking.Networks;
import org.servalproject.servalchat.R;
import org.servalproject.servalchat.navigation.MainActivity;
import android.Manifest;

import agency.tango.android.avatarview.utils.StringUtils;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by rodrigobrauwers on 15/02/18.
 */

public class GetStartedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);
        setSupportActionBar((Toolbar) findViewById(R.id.app_toolbar));
        setTitle(R.string.get_started);
        stepOne();
    }

    public void saveIdentityName(final String name) {
        final SharedPreferences pref = getApplicationContext().getSharedPreferences(SharedPrefsKeys.IDENTITY_PREFS, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SharedPrefsKeys.IDENTITY_NAME_TMP, name);
        editor.apply();
        startMainActivity();
    }

    public void createIdentity(final String name) {
        if (StringUtils.isNullOrEmpty(name)) {
            Toast.makeText(this, R.string.name_error, Toast.LENGTH_SHORT).show();
            return;
        }

        final SharedPreferences pref = getApplicationContext().getSharedPreferences(SharedPrefsKeys.IDENTITY_PREFS, 0);

        new BackgroundWorker() {
            @Override
            protected void onBackGround() throws Exception {
                Serval.getInstance().identities.addIdentity("", name, "");
            }

            @Override
            protected void onComplete(Throwable t) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("identity_name", name);
                editor.apply();
                startMainActivity();
            }
        }.execute();
    }

    public void stepOne() {
        replaceFragment(new StepOneFragment(), "One");
    }

    public void stepTwo() {
        replaceFragment(new PagerFragment(), "Pager");
    }

    public void stepThree() {
        replaceFragment(new StepThreeFragment(), "SignUp");
    }

    private void replaceFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_container, fragment).addToBackStack(tag).commit();
    }

    private void startMainActivity() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
    }

    public static class StepOneFragment extends Fragment {

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_get_started_1, container, false);
            view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        ((GetStartedActivity) activity).stepTwo();
                    }
                }
            });
            return view;
        }
    }

    public static class StepThreeFragment extends Fragment {

        private EditText editText;
        private Button button;
        private ProgressBar progressBar;

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_get_started_3, container, false);
            editText = view.findViewById(R.id.editText);
            button = view.findViewById(R.id.button);
            progressBar = view.findViewById(R.id.progressBar);

            editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int action, KeyEvent keyEvent) {
                    if (action == EditorInfo.IME_ACTION_DONE) {
                        progressBar.setVisibility(View.VISIBLE);
                        button.setEnabled(false);
                        MainActivity.hideKeyboard((AppCompatActivity) getActivity());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                saveIdentityName();
                            }
                        }, 500);
                    }
                    return false;
                }
            });

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    saveIdentityName();
                }
            });

            return view;
        }

        private void saveIdentityName() {
            progressBar.setVisibility(View.VISIBLE);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                String text = editText.getText() == null ? null : editText.getText().toString();
                if (StringUtils.isNullOrEmpty(text)) {
                    Toast.makeText(activity, R.string.name_error, Toast.LENGTH_SHORT).show();
                    button.setEnabled(true);
                    progressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    button.setEnabled(false);
                    ((GetStartedActivity) activity).saveIdentityName(text);
                }
            }
        }
    }

    public static class PagerFragment extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener {

        private ViewPager viewPager;
        private Button skipButton, continueButton;

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_get_started_2, container, false);

            skipButton = view.findViewById(R.id.skipButton);
            skipButton.setOnClickListener(this);
            continueButton = view.findViewById(R.id.continueButton);
            continueButton.setOnClickListener(this);

            viewPager = view.findViewById(R.id.viewPager);
            viewPager.setAdapter(new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()));
            viewPager.addOnPageChangeListener(this);

            CircleIndicator indicator = view.findViewById(R.id.indicator);
            indicator.setViewPager(viewPager);

            return view;
        }

        @Override
        public void onClick(View view) {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                return;
            }

            switch (view.getId()) {
                case R.id.skipButton:
                    ((GetStartedActivity) activity).stepThree();
                    break;

                case R.id.continueButton:
                    int currentItem = viewPager.getCurrentItem();
                    if (currentItem == 2) {
                        ((GetStartedActivity) activity).stepThree();
                    }
                    else {
                        viewPager.setCurrentItem(currentItem+1, true);
                    }
                    break;
            }
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            updateUI(position);
        }

        @Override
        public void onPageSelected(int position) {
            updateUI(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

        private void updateUI(int position) {
            //continueButton.setVisibility(position == 2 ? View.INVISIBLE : View.VISIBLE);
        }
    }

    public static class PageFragment extends Fragment {
        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_get_started_2_1, container, false);

            TextView titleTextView = view.findViewById(R.id.titleTextView);
            TextView textView = view.findViewById(R.id.textView);
            int index = getArguments().getInt("index");

            switch (index) {
                case 0:
                    titleTextView.setText(R.string.alpha_instructions_5);
                    textView.setText(R.string.alpha_instructions_2);
                    break;

                case 1:
                    titleTextView.setText(R.string.alpha_instructions_6);
                    textView.setText(R.string.alpha_instructions_3);
                    break;

                case 2:
                    titleTextView.setText(R.string.alpha_instructions_7);
                    textView.setText(R.string.alpha_instructions_4);
                    break;
            }

            return view;
        }
    }

    public static class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            PageFragment f = new PageFragment();
            Bundle args = new Bundle();
            args.putInt("index", position);
            f.setArguments(args);
            return f;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

}
