package org.servalproject.servalchat.views;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.servalproject.servalchat.ContactItemList;
import org.servalproject.servalchat.R;

import java.util.ArrayList;

import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;

/**
 * Created by rodrigobrauwers on 15/02/18.
 */

public class ContactAdapter2 extends RecyclerView.Adapter<ContactAdapter2.ContactViewHolder> {

    private ArrayList<ContactItemList> contacts;
    private GlideLoader glideLoader;

    public ContactAdapter2(ArrayList<ContactItemList> contacts, GlideLoader glideLoader) {
        this.contacts = contacts;
        this.glideLoader = glideLoader;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_contact, parent, false);
        return new ContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder vh, int position) {
        ContactItemList contact = contacts.get(position);
        vh.titleTextView.setText(contact.getName());
        vh.subtitleTextView.setText(contact.getPhNumber());
        glideLoader.loadImage(vh.avatarView, "", contact.getName());
    }

    @Override
    public int getItemCount() {
        return contacts == null ? 0 : contacts.size();
    }

    protected static class ContactViewHolder extends RecyclerView.ViewHolder {
        public AvatarView avatarView;
        public TextView titleTextView, subtitleTextView;

        public ContactViewHolder(View itemView) {
            super(itemView);
            avatarView = itemView.findViewById(R.id.avatarView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            subtitleTextView = itemView.findViewById(R.id.subtitleTextView);
        }

    }

}
