package org.servalproject.servalchat.views;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.servalproject.mid.Serval;
import org.servalproject.servalchat.R;
import org.servalproject.servalchat.navigation.Events;

import java.lang.ref.WeakReference;

/**
 * Created by rodrigobrauwers on 14/02/18.
 */

public class BottomNavigationListener implements BottomNavigationView.OnNavigationItemSelectedListener{

    private WeakReference<AppCompatActivity> activityRef;
    private boolean finishActivity;
    private int currentOption;

    public BottomNavigationListener(AppCompatActivity activity, boolean finishActivity, int currentOption) {
        activityRef = new WeakReference<>(activity);
        this.finishActivity = finishActivity;
        this.currentOption = currentOption;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return onNavigationItemSelected(item.getItemId());
    }

    public boolean onNavigationItemSelected(int itemId) {
        if (itemId == currentOption) {
            return false;
        }

        currentOption = itemId;

        if (finishActivity) {
            AppCompatActivity activity = activityRef.get();
            if (activity != null) {
                activity.finish();
            }
        }

        switch (itemId) {
            /*
            case R.id.navigation_contacts:
                EventBus.getDefault().post(new Events.BottomNavigationSelected(R.id.ll_contact));
                break;
            */

            case R.id.navigation_chats:
                EventBus.getDefault().post(new Events.BottomNavigationSelected(R.id.ll_message));
                break;

            case R.id.navigation_near_me:
                EventBus.getDefault().post(new Events.BottomNavigationSelected(R.id.navigation_near_me));
                break;

            /*
            case R.id.navigation_calls:
                break;
            */

            case R.id.navigation_wallet:
                showToast(R.string.coming_soon);
                return false;
        }

        return true;
    }

    private void showToast(int textId) {
        AppCompatActivity activity = activityRef.get();
        if (activity != null) {
            Toast.makeText(activity, textId, Toast.LENGTH_SHORT).show();
        }
    }

    public int getCurrentOption() {
        return currentOption;
    }
}