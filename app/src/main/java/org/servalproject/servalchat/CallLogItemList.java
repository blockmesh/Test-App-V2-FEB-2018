package org.servalproject.servalchat;

public class CallLogItemList
{

    String phNumber,dir,callDayTime,callDuration,name;

    public CallLogItemList(){}

    public CallLogItemList(String name,String phNumber, String dir, String callDayTime,String callDuration) {
        this.name= name;
        this.phNumber= phNumber;
        this.dir= dir;
        this.callDayTime=callDayTime;
        this.callDuration=callDuration;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPhNumber() {
        return phNumber;
    }

    public void setPhNumber(String phNumber) {
        this.phNumber = phNumber;
    }


    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }


    public String getCallDayTime() {
        return callDayTime;
    }

    public void setCallDayTime(String callDayTime) {
        this.callDayTime = callDayTime;
    }

    public String getCallDuration() {
        return callDuration;
    }

    public void setCallDuration(String callDuration) {
        this.callDuration = callDuration;
    }

}
