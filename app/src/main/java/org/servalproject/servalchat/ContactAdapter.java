package org.servalproject.servalchat;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import android.widget.Filter;
import android.widget.Filterable;

public class ContactAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<ContactItemList> arr;
    private ArrayList<ContactItemList> contactItemLists;
    ArrayList<ContactItemList> FilteredList = new ArrayList<ContactItemList>();
    private Boolean type;
    boolean isFilter = false;
    public ContactAdapter(Context context, ArrayList<ContactItemList> callLogItemList, Boolean type) {
        mContext = context;
        this.contactItemLists = callLogItemList;
        this.type= type;
    }

    @Override
    public int getCount() {
        return contactItemLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


//        if (isFilter) {
//            contactItemLists = FilteredList.get(position);
//        } else {
//            contactItemLists = arr.get(position);
//        }
        if (convertView == null) {


            grid = new View(mContext);
            grid = inflater.inflate(R.layout.call_log_list, null);

            LinearLayout llRight = (LinearLayout) grid.findViewById(R.id.ll_right);
            TextView  tvNumber = (TextView) grid.findViewById(R.id.tv_call_number);
            TextView tvName = (TextView) grid.findViewById(R.id.tv_call_name);

            llRight.setVisibility(View.GONE);

            tvName.setText(contactItemLists.get(position).getName());
            tvNumber.setText(contactItemLists.get(position).getPhNumber());

        } else {
            grid = (View) convertView;

            LinearLayout  llRight = (LinearLayout) grid.findViewById(R.id.ll_right);
            TextView  tvNumber = (TextView) grid.findViewById(R.id.tv_call_number);
            TextView tvName = (TextView) grid.findViewById(R.id.tv_call_name);
            llRight.setVisibility(View.GONE);
            tvName.setText(contactItemLists.get(position).getName());
            tvNumber.setText(contactItemLists.get(position).getPhNumber());

        }


        return grid;
    }

//    @Override
//    public Filter getFilter() {
//        // TODO Auto-generated method stub
//        Filter filter = new Filter() {
//
//            @Override
//            protected void publishResults(CharSequence constraint,
//                                          FilterResults results) {
//                // TODO Auto-generated method stub
//                arr = (ArrayList<ContactItemList>) results.values;
//                notifyDataSetChanged();
//            }
//
//            @Override
//            protected FilterResults performFiltering(CharSequence constraint) {
//                // TODO Auto-generated method stub
//                FilterResults results = new FilterResults();
//
//                if (constraint == null || constraint.length() == 0) {
//                    // No filter implemented we return all the list
//                    results.values = contactItemLists;
//                    results.count = contactItemLists.size();
//
//                    isFilter = false;
//
//                } else {
//
//                    FilteredList.clear();
//                    isFilter = true;
//                    Log.e("San"," name");
//                    for (int i = 0; i < contactItemLists.size(); i++) {
//
//                        ContactItemList data1 = contactItemLists.get(i);
//
//
//                        if (data1.name.toString().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
//                            FilteredList.add(data1);
//
//                            Log.e("San",data1.name.toString()+" name");
//                        }
//
//                    }
//
//                    results.values = FilteredList;
//                    results.count = FilteredList.size();
//
//                }
//                return results;
//            }
//        };
//        return filter;
//    }



}