package org.servalproject.servalchat.navigation;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.transitionseverywhere.Recolor;
import com.transitionseverywhere.TransitionManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.servalproject.RequestCodes;
import org.servalproject.SharedPrefsKeys;
import org.servalproject.mid.FeedList;
import org.servalproject.mid.Identity;
import org.servalproject.mid.IdentityFeed;
import org.servalproject.mid.ListObserver;
import org.servalproject.mid.MessageList;
import org.servalproject.mid.Messaging;
import org.servalproject.mid.Peer;
import org.servalproject.mid.Serval;
import org.servalproject.mid.networking.Networks;
import org.servalproject.servalchat.App;
import org.servalproject.servalchat.CallLogActivity;
import org.servalproject.servalchat.ConnectActivity;
import org.servalproject.servalchat.ContactActivity;
import org.servalproject.servalchat.ContactFragment;
import org.servalproject.servalchat.NonSwipeableViewPager;
import org.servalproject.servalchat.R;
import org.servalproject.servalchat.feeds.FeedListAdapter;
import org.servalproject.servalchat.feeds.PublicFeedsList;
import org.servalproject.servalchat.feeds.PublicFeedsPresenter;
import org.servalproject.servalchat.identity.IdentityDetailsPresenter;
import org.servalproject.servalchat.peer.PeerList;
import org.servalproject.servalchat.peer.PrivateMessaging;
import org.servalproject.servalchat.views.BackgroundWorker;
import org.servalproject.servalchat.views.BottomNavigationListener;
import org.servalproject.servalchat.views.BottomNavigationViewHelper;
import org.servalproject.servaldna.Subscriber;
import org.servalproject.servaldna.meshmb.MeshMBCommon;
import org.servalproject.servaldna.rhizome.RhizomeListBundle;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;

public class MainActivity extends AppCompatActivity implements IContainerView, MenuItem.OnMenuItemClickListener, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";
    private App app;
    private IRootContainer rootContainer;
    private NavHistory history;
    public static Serval serval;
    public static Identity identity;
    public static String myIdentity = "";
    public static Peer peer;
    private InputMethodManager imm;
    private List<Identity> identities = new ArrayList<>();
    private IdentityFeed feed;
    private Navigation navigation;
    public NonSwipeableViewPager pager;
    public TabLayout slidingTabs;
    MainActivity activity;
    ImageView icon, imgMore, imgHeaderBgArrow, imgHeaderLogo, imgCall, imgContact, imgMessage, imgMaps, imgShareFile, imgShareUs, imgSetting, imgConnect, imgHelp, imgWallet;
    TextView sidLabel, tvHeader, txtTitle, txtCall, txtContact, txtMessage, txtMaps, txtShareFile, txtShareUs, txtSetting, txtConnect, txtHelp, txtCommingSoon;
    TextView sid;
    EditText name;
    Button update, cancle;
    IdentityDetailsPresenter presenter;
    String identityName;
    Dialog dialog;
    public static int width, height;
    public double ww = 0, hh = 0;
    public RelativeLayout llCenter;
    public LinearLayout llTop, llContact, llCall, llMessage, llMaps, llShareFile, llShareUs, llSetting, llConnect, llHelp, llBottom;
    Toolbar appToolbar;
    private MessageList messages;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    public static Context contextMain;
    private View alphaInstructionsContainer;
    private Button alphaInstructionsContinueButton;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private BottomNavigationView bottomNavigationView;
    private BottomNavigationListener bottomNavigationListener;
    private NavigationView navigationView;
    private ProgressBar horizontalProgressBar;
    private TextView buildingMeshTextView;
    private TextView noContactsTextView;
    private boolean startingUp = true, chatActive;
    private Peer currentPeerDoingChat;

    // Content views
    private PublicFeedsList publicFeedsListView;
    private PeerList peerListView;
    private PrivateMessaging chatView;

    public static List<String> mLivePeerList = new ArrayList<String>();
    private List<RhizomeListBundle> meshContacts = new ArrayList<>();

    private boolean hasFeeds, enablingDiscoverabilityFirstAccess;
    private ScheduledExecutorService executor;

    // Container where the content is placed (serval custom views, framents, etc)
    private FrameLayout contentContainer;

    // The parent of contentContainer and bottomNavigationView
    private ConstraintLayout contentParent;

    // The current serval custom view (e.g PublicFeedsList) (semi-deprecated)
    private View contentView;

    public static void hideKeyboard(AppCompatActivity activity) {
        if (activity == null) {
            Log.d(TAG, "hideKeyboard: early return[1]");
            return;
        }

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager == null) {
            Log.d(TAG, "hideKeyboard: early return[2]");
            return;
        }

        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }

        inputMethodManager.hideSoftInputFromInputMethod(view.getWindowToken(), 0);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");

        super.onCreate(savedInstanceState);
        app = (App) getApplication();
        app.setContext(getApplicationContext());
//		Log.e("San"," here MainActivity context");

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        serval = Serval.getInstance();

        Intent intent = getIntent();
        init(intent, savedInstanceState);

        contextMain = getApplicationContext();
        // Create custom dialog object
        pref = getApplicationContext().getSharedPreferences(SharedPrefsKeys.IDENTITY_PREFS, 0);
        identityName = pref.getString("identity_name", null);

        identities = serval.identities.getIdentities();

        mLivePeerList.clear();
        addReachablesPeers();

//		Log.e("San","known Peer"+serval.knownPeers.getReachablePeers().size());

//		messages = MainActivity.identity.messaging.getPrivateMessages(subscriber);
//

//		Log.e("San","getReachablePeers :- "+serval.knownPeers.getReachablePeers());
//				for (int u=0; u<identities.size(); u++)
//				{
//
//					FeedList feedList = identities.get(u).getAllFeeds();
//				}

        if (identityName != null) {
            identity = identities.get(0);
            /*
            identity.getId();
            Log.e("San","identity.getId :- "+identity.getId());
            Log.e("San","identity.getId :- "+identity.getAllFeeds());
            Log.e("San","Main identity:- "+identity);
            Log.e("San","identity:- "+identity+" Id:- "+identity.getId()+" DId:-"+identity.getDid()+" Name:- "+identity.getName()+" subscriber"+identity.subscriber);
            */

            myIdentity = identity.subscriber.toString();
        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;


        ww = width / 3;
        hh = height / 5;


        /*
        if ((int) Build.VERSION.SDK_INT < 23) {
            newIdentity();
        } else {
            if (PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG)
                    == PackageManager.PERMISSION_DENIED) {
                //Log.e("hello", "read call log permission called");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALL_LOG},
                        120);
            } else if (PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                    == PackageManager.PERMISSION_DENIED) {
                //Log.e("hello", "read contacts permission called");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS},
                        130);
            } else if (PermissionChecker.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_DENIED) {
                //Log.e("hello", "call phone permission called");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},
                        140);
            } else if (PermissionChecker.checkSelfPermission(this, Manifest.permission.WRITE_CALL_LOG)
                    == PackageManager.PERMISSION_DENIED) {
                //Log.e("hello", "call phone permission called");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CALL_LOG},
                        150);
            } else {
                newIdentity();
            }
        }
        */

        slidingTabs = (TabLayout) findViewById(R.id.sliding_tabs);
        pager = (NonSwipeableViewPager) findViewById(R.id.pager);

//		pager.requestDisallowInterceptTouchEvent(true);
//
//		pager.setOnTouchListener(new View.OnTouchListener()
//		{
//			@Override
//			public boolean onTouch(View v, MotionEvent event)
//			{
//				return true;
//			}
//		});


        imgMore = (ImageView) findViewById(R.id.img_more);
        imgHeaderBgArrow = (ImageView) findViewById(R.id.img_header_bg_arrow);
        imgHeaderLogo = (ImageView) findViewById(R.id.img_header_logo);
        txtTitle = (TextView) findViewById(R.id.txt_title);
        txtCall = (TextView) findViewById(R.id.txt_call);
        txtContact = (TextView) findViewById(R.id.txt_contact);
        txtMessage = (TextView) findViewById(R.id.txt_message);
        txtMaps = (TextView) findViewById(R.id.txt_maps);
        txtShareFile = (TextView) findViewById(R.id.txt_share_file);
        txtShareUs = (TextView) findViewById(R.id.txt_share_us);
        txtSetting = (TextView) findViewById(R.id.txt_setting);
        txtConnect = (TextView) findViewById(R.id.txt_connect);
        txtHelp = (TextView) findViewById(R.id.txt_help);
        txtCommingSoon = (TextView) findViewById(R.id.txt_comming_soon);

        llCenter = (RelativeLayout) findViewById(R.id.ll_center);
        llTop = (LinearLayout) findViewById(R.id.ll_top);
        llCall = (LinearLayout) findViewById(R.id.ll_call);
        llContact = (LinearLayout) findViewById(R.id.ll_contact);
        llMessage = (LinearLayout) findViewById(R.id.ll_message);
        llMaps = (LinearLayout) findViewById(R.id.ll_maps);
        llShareFile = (LinearLayout) findViewById(R.id.ll_share_file);
        llShareUs = (LinearLayout) findViewById(R.id.ll_share_us);
        llSetting = (LinearLayout) findViewById(R.id.ll_setting);
        llConnect = (LinearLayout) findViewById(R.id.ll_connect);
        llHelp = (LinearLayout) findViewById(R.id.ll_help);
        llBottom = (LinearLayout) findViewById(R.id.ll_bottom);
        imgCall = (ImageView) findViewById(R.id.img_call);
        imgContact = (ImageView) findViewById(R.id.img_contact);
        imgMessage = (ImageView) findViewById(R.id.img_message);
        imgMaps = (ImageView) findViewById(R.id.img_maps);
        imgShareFile = (ImageView) findViewById(R.id.img_share_file);
        imgShareUs = (ImageView) findViewById(R.id.img_share_us);
        imgSetting = (ImageView) findViewById(R.id.img_setting);
        imgConnect = (ImageView) findViewById(R.id.img_connect);
        imgHelp = (ImageView) findViewById(R.id.img_help);
        imgWallet = (ImageView) findViewById(R.id.img_wallet);
        appToolbar = (Toolbar) findViewById(R.id.app_toolbar);
        alphaInstructionsContainer = findViewById(R.id.alpha_instructions_container);
        alphaInstructionsContinueButton = findViewById(R.id.alphaInstructionsContinueButton);
        contentContainer = findViewById(R.id.content_container);
        contentParent = findViewById(R.id.content_parent);
        horizontalProgressBar = findViewById(R.id.horizontalProgressBar);
        buildingMeshTextView = findViewById(R.id.buildingMeshTextView);
        noContactsTextView = findViewById(R.id.noContactsTextView);
        //publicFeedsListView = findViewById(R.id.publicFeedListView);
        //peerListView = findViewById(R.id.peerListView);

        llCall.setOnClickListener(this);
        llMessage.setOnClickListener(this);
        llSetting.setOnClickListener(this);
        llShareUs.setOnClickListener(this);
//		llShareFile.setOnClickListener(this);
        llConnect.setOnClickListener(this);
        llContact.setOnClickListener(this);
        llMaps.setOnClickListener(this);
        alphaInstructionsContinueButton.setOnClickListener(this);

//		llHelp.setOnClickListener(this);

        appToolbar.setVisibility(View.VISIBLE);
        llCenter.setVisibility(View.VISIBLE);
        setSupportActionBar(appToolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, appToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        actionBarDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24px);
        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, String.format("ToolbarNavigationClick DrawerIndicatorEnabled: %b", actionBarDrawerToggle.isDrawerIndicatorEnabled()));
                if (!actionBarDrawerToggle.isDrawerIndicatorEnabled()) {
                    onBackPressed();
                }
            }
        });
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);
        bottomNavigationListener = new BottomNavigationListener(this, false, -1);
        bottomNavigationView.setOnNavigationItemSelectedListener(bottomNavigationListener);
        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {
                if (startingUp) {
                    bottomNavigationListener.onNavigationItemSelected(item);
                }
            }
        });
        bottomNavigationView.getMenu().setGroupEnabled(R.id.navigation_group, false); // We have a delay to Serval startup

        onUiSet();

        EventBus.getDefault().register(this);

        /*
        if (identityName != null) {
            onIdentitySet();
        }
        */

        if (identityName != null) {
            onIdentitySet(false);
        }
        else {
            createIdentity(getApplicationContext().getSharedPreferences(SharedPrefsKeys.IDENTITY_PREFS, 0).getString(SharedPrefsKeys.IDENTITY_NAME_TMP, null));
        }

        /*
        if (hasContactsPermission()) {
            onContactsPermissionGranted();
        }
        else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS},130);
        }
        */

        if (!hasContactsPermission()) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_COARSE_LOCATION},200);
        }

        /*
        try {
            SimpleDateFormat f1 = new SimpleDateFormat("z", Locale.US);
            SimpleDateFormat f2 = new SimpleDateFormat("zz", Locale.US);
            SimpleDateFormat f3 = new SimpleDateFormat("zzz", Locale.US);
            SimpleDateFormat f4 = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
            Date date = new Date();
            String date2 = "Thu Feb 15 09:26:48 GMT-02:00 2018";

            Log.d(TAG, String.format("date1: %s", f1.format(date)));
            Log.d(TAG, String.format("date2: %s", f2.format(date)));
            Log.d(TAG, String.format("date3: %s", f3.format(date)));
            Log.d(TAG, String.format("date4: %s", f4.format(date)));
            Log.d(TAG, String.format("date5: %s", f4.parse(date2)));
            Log.d(TAG, String.format("date6: %s", f4.format(f4.parse(date2))));
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }

    private boolean hasCoarseLocationPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean hasContactsPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;
    }

    public void addReachablesPeers() {
        if (serval.knownPeers.getReachablePeers().size() > 0) {
            for (Peer p : serval.knownPeers.getReachablePeers()) {
                addPeer(p);
            }
        }
    }

    public void addPeer(Peer peer) {
//		Log.e("San","peer.getName:- "+peer.getName()+"");
//		Log.e("San","peer.getSubscriber:- "+peer.getSubscriber()+"");
        if (peer.getName() != null && !mLivePeerList.contains(peer.getSubscriber()+"")) {
            mLivePeerList.add(peer.getSubscriber() + "");
//				messages = identity.messaging.getPrivateMessages(identity.subscriber);
//				Log.e("San", "Main self.sid " + messages.self.sid);
//				Log.e("San", "Main peer.sid " + peer.getSubscriber().sid+ "");
//
////				Subscriber peerId = (Subscriber) peer.getSubscriber().sid;
//				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//				StrictMode.setThreadPolicy(policy);
//
//				try {
//					MeshMSMessageList meshMSMessageList = MainActivity.serval.getResultClient().meshmsListMessages(messages.self.sid, peer.getSubscriber().sid);
//					int msgCount = meshMSMessageList.toList().size();
//					Log.e("San","msgCount:- "+msgCount+"");
//					if (msgCount > 0) {
//						Log.e("San","msgCount...:- "+msgCount+"");
//						IObservableList list = messages;
//						MeshMSMessage msg = (MeshMSMessage) list.next();
//
//						Log.e("San", "Main msg " + msg.text);
//
//					}
//
//				} catch (ServalDInterfaceException e) {
//					e.printStackTrace();
//				} catch (IOException e) {
//					e.printStackTrace();
//				} catch (MeshMSException e) {
//					e.printStackTrace();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
        }
//		Log.e("San","peer;;;;:- "+peer.getName()+"");

    }

    private void createIdentity(final String name) {
        Log.d(TAG, "createIdentity: " + name);

        new BackgroundWorker() {
            private Identity result;

            @Override
            protected void onBackGround() throws Exception {
                Serval serval = Serval.getInstance();
                if (identity == null) {
                    result = serval.identities.addIdentity("", name, "");
                    identities = serval.identities.getIdentities();
                    identity = identities.get(0);
                    myIdentity = identity.subscriber.toString();
                    identityName = identity.getName();
                } else {
                    serval.identities.updateIdentity(identity, "", name, "");
                    result = identity;
                }
                feed = result.getFeed();
                feed.sendMessage("Demo feed");
            }

            @Override
            protected void onComplete(Throwable t) {
                editor = pref.edit();
                editor.putString("identity_name", name);
                editor.apply();
                onIdentitySet(true);
            }
        }.execute();
    }

    public void newIdentity() {
        // Migrated to GetStarted
        if (true) {
            return;
        }

        dialog = new Dialog(MainActivity.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.identity_dialog);
        // Set dialog title
        dialog.setTitle("NewDetails");
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        icon = (ImageView) dialog.findViewById(R.id.identicon);
        tvHeader = (TextView) dialog.findViewById(R.id.tv_header);
        sid = (TextView) dialog.findViewById(R.id.sid);
        sidLabel = (TextView) dialog.findViewById(R.id.sid_label);
        name = (EditText) dialog.findViewById(R.id.name);
        update = (Button) dialog.findViewById(R.id.update);
        cancle = (Button) dialog.findViewById(R.id.cancle);
        update.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 8);
        dialog.setCanceledOnTouchOutside(false);
        if (identityName == null) {
            dialog.show();

            sidLabel.setVisibility(View.GONE);
            sid.setVisibility(View.GONE);
            icon.setVisibility(View.GONE);
            update.setText(R.string.add_identity);
            if (App.isTesting())
                name.setText("Test User");
        }


        final boolean isNew = identity == null;

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String names = name.getText().toString();
                if (names == null || names.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Identity can not be empty", Toast.LENGTH_SHORT).show();
                } else {
                    new BackgroundWorker() {
                        private Identity result;

                        @Override
                        protected void onBackGround() throws Exception {
                            Serval serval = Serval.getInstance();
                            if (identity == null) {
                                result = serval.identities.addIdentity("", names, "");
                                identities = serval.identities.getIdentities();
                                identity = identities.get(0);
                                myIdentity = identity.subscriber.toString();
                                identityName = identity.getName();
                            } else {
                                serval.identities.updateIdentity(identity, "", names, "");
                                result = identity;
                            }
                            feed = result.getFeed();
                            feed.sendMessage("Demo feed");
                        }

                        @Override
                        protected void onComplete(Throwable t) {

//							pref = getApplicationContext().getSharedPreferences(SharedPrefsKeys.IDENTITY_PREFS, 0);
                            editor = pref.edit();
                            editor.putString("identity_name", names);
                            editor.commit();
//							if (identity == null)
//								go(Navigation.MyFeed, result, null, null, true);
//							else
//								go(Navigation.MyFeed);

//                        dialog.dismiss();
                            dialog.hide();

                            if (isNew) {
                                alphaInstructionsContainer.setVisibility(View.VISIBLE);
                                onIdentitySet(true);
                            }
                            else {
                                updateNavHeader();
                            }
//						}
                        }
                    }.execute();
                }
            }
        });

        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
                dialog.dismiss();
            }
        });

    }


    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        updateNoContactsVisibility();
    }

    private void onContactsPermissionGranted() {
        //bottomNavigationView.setSelectedItemId(R.id.navigation_contacts);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, String.format("onActivityResult %d enablingDiscoverabilityFirstAccess: %b", requestCode, enablingDiscoverabilityFirstAccess));
        if (requestCode == RequestCodes.BLUETOOTH_DISCOVERABLE && enablingDiscoverabilityFirstAccess) {
            enablingDiscoverabilityFirstAccess = false;
            enableBottomBar(true);
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void onIdentitySet(boolean isNew) {
        updateNavHeader();
        Networks networks = Networks.getInstance();

        if (isNew && !networks.blueTooth.isDiscoverable()) {
            enablingDiscoverabilityFirstAccess = true;
            if (networks.wifiHotspot != null) {
                networks.wifiHotspot.disable(this);
            }

            networks.wifiClient.disable(this);
            networks.blueTooth.setEnabled(true);
            networks.blueTooth.requestDiscoverable(this);
        }
        else {
            enableBottomBar(false);
        }
    }

    private void enableBottomBar(boolean extendedDelay) {
        final int delay = extendedDelay ? 20000 : 12000;

        /*
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(200);
        anim.setStartOffset(500);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        buildingMeshTextView.startAnimation(anim);
        */

        new CountDownTimer(delay, 100) {
            public void onTick(long millisUntilFinished) {
                float df = delay;
                float mf = millisUntilFinished;
                int progress = (int) (((df - mf) / df) * 100f);
                horizontalProgressBar.setProgress(progress);
            }

            public void onFinish() {
                buildingMeshTextView.clearAnimation();
                horizontalProgressBar.setVisibility(View.INVISIBLE);
                buildingMeshTextView.setVisibility(View.INVISIBLE);
            }
        }.start();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                createContentViews();
                bottomNavigationView.getMenu().setGroupEnabled(R.id.navigation_group, true);
                bottomNavigationView.setSelectedItemId(R.id.navigation_chats);
                startingUp = false;
                invalidateOptionsMenu();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateNoContactsVisibility();
                    }
                }, 1000);
            }
        }, delay);
    }

    private void updateNavHeader() {
        View header = navigationView.getHeaderView(0);
        GlideLoader glideLoader = new GlideLoader();
        AvatarView avatarView = header.findViewById(R.id.nav_header_image_view);
        TextView titleView = header.findViewById(R.id.nav_header_title_view);
        TextView subtitleView = header.findViewById(R.id.nav_header_subtitle_view);

        glideLoader.loadImage(avatarView, "", identityName);
        titleView.setText(identityName);
        subtitleView.setText("+00 000 0000");
    }

    public void onUiSet() {

        // By kine
        if (true) {
            return;
        }

        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) hh);
        llTop.setLayoutParams(parms);


        LinearLayout.LayoutParams parmsCall = new LinearLayout.LayoutParams((int) ww, (int) hh - 10);
        llCall.setLayoutParams(parmsCall);

        LinearLayout.LayoutParams parmsMessage = new LinearLayout.LayoutParams((int) ww, (int) hh - 10);
        llMessage.setLayoutParams(parmsMessage);

        LinearLayout.LayoutParams parmsContact = new LinearLayout.LayoutParams((int) ww, (int) hh - 10);
        llContact.setLayoutParams(parmsContact);

        LinearLayout.LayoutParams parmsMaps = new LinearLayout.LayoutParams((int) ww, (int) hh - 10);
        llMaps.setLayoutParams(parmsMaps);

        LinearLayout.LayoutParams parmsShareFile = new LinearLayout.LayoutParams((int) ww, (int) hh - 10);
        llShareFile.setLayoutParams(parmsShareFile);

        LinearLayout.LayoutParams parmsShareUs = new LinearLayout.LayoutParams((int) ww, (int) hh - 10);
        llShareUs.setLayoutParams(parmsShareUs);


        LinearLayout.LayoutParams parmsSettings = new LinearLayout.LayoutParams((int) ww, (int) hh - 10);
        llSetting.setLayoutParams(parmsSettings);

        LinearLayout.LayoutParams parmsConnect = new LinearLayout.LayoutParams((int) ww, (int) hh - 10);
        llConnect.setLayoutParams(parmsConnect);

        LinearLayout.LayoutParams parmsHelp = new LinearLayout.LayoutParams((int) ww, (int) hh - 10);
        llHelp.setLayoutParams(parmsHelp);

        LinearLayout.LayoutParams parmsBottom = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) hh);
        llBottom.setLayoutParams(parmsBottom);

//		go(Navigation.MyFeed);

//		llHelp.setAlpha(0.4f);


        RelativeLayout.LayoutParams parmsImgMore = new RelativeLayout.LayoutParams((int) hh / 3, (int) hh / 3);
        imgMore.setPadding((int) hh / 13, (int) hh / 13, (int) hh / 13, (int) hh / 13);
        imgMore.setLayoutParams(parmsImgMore);

        RelativeLayout.LayoutParams parmsImgHeaderBgArrow = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) hh / 3);
        imgHeaderBgArrow.setLayoutParams(parmsImgHeaderBgArrow);

        RelativeLayout.LayoutParams parmsImgHeaderLogo = new RelativeLayout.LayoutParams(((int) hh / 6) * 5, ((int) hh / 6) * 4);
        parmsImgHeaderLogo.setMargins(0, (int) hh / 13, 0, 0);
        parmsImgHeaderLogo.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        imgHeaderLogo.setLayoutParams(parmsImgHeaderLogo);

        txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 5);
        txtCall.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 8);
        txtContact.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 8);
        txtMessage.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 8);
        txtMaps.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 8);
        txtShareFile.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 8);
        txtShareUs.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 8);
        txtSetting.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 8);
        txtConnect.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 8);
        txtHelp.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 8);
        txtCommingSoon.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) hh / 10);

        LinearLayout.LayoutParams parmsImgCall = new LinearLayout.LayoutParams((int) ww, ((int) hh / 6) * 4);
        imgCall.setLayoutParams(parmsImgCall);

        LinearLayout.LayoutParams parmsImgContact = new LinearLayout.LayoutParams((int) ww, ((int) hh / 6) * 4);
        imgContact.setLayoutParams(parmsImgContact);

        LinearLayout.LayoutParams parmsImgMessage = new LinearLayout.LayoutParams((int) ww, ((int) hh / 6) * 4);
        imgMessage.setLayoutParams(parmsImgMessage);


        LinearLayout.LayoutParams parmsImgMaps = new LinearLayout.LayoutParams((int) ww, ((int) hh / 6) * 4);
        imgMaps.setLayoutParams(parmsImgMaps);

        LinearLayout.LayoutParams parmsImgShareFile = new LinearLayout.LayoutParams((int) ww, ((int) hh / 6) * 4);
        imgShareFile.setLayoutParams(parmsImgShareFile);

        LinearLayout.LayoutParams parmsImgShareUs = new LinearLayout.LayoutParams((int) ww, ((int) hh / 6) * 4);
        imgShareUs.setLayoutParams(parmsImgShareUs);


        LinearLayout.LayoutParams parmsImgSetting = new LinearLayout.LayoutParams((int) ww, ((int) hh / 6) * 4);
        imgSetting.setLayoutParams(parmsImgSetting);

        LinearLayout.LayoutParams parmsImgConnect = new LinearLayout.LayoutParams((int) ww, ((int) hh / 6) * 4);
        imgConnect.setLayoutParams(parmsImgConnect);

        LinearLayout.LayoutParams parmsImgHelp = new LinearLayout.LayoutParams((int) ww, ((int) hh / 6) * 4);
        imgHelp.setLayoutParams(parmsImgHelp);

        LinearLayout.LayoutParams parmsImgWallet = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ((int) hh / 7) * 4);
        imgWallet.setLayoutParams(parmsImgWallet);

    }

    private void startContactsActivity(boolean addToBackStack) {
        Intent i = new Intent(getApplicationContext(), ContactActivity.class);

        if (!addToBackStack) {
            i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        }

        startActivity(i);
    }

    @Override
    public void onClick(View v) {
        onClick(v.getId());
    }

    private void onClick(int viewId) {
        Intent i;
        switch (viewId) {
            case R.id.ll_share_us:
                /*
				if (true) {
					alphaInstructionsContainer.setVisibility(View.VISIBLE);
					return;
				}
				*/

                //        For Share app:
                final String appPackageName = getApplicationContext().getPackageName(); // getPackageName()
                // from Context or Activity object

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Download " + getResources().getString(R.string.app_name));
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case R.id.ll_connect:

                i = new Intent(getApplicationContext(), ConnectActivity.class);
                startActivity(i);

                break;
            case R.id.ll_maps:
                // llCenter.setVisibility(View.GONE);
                // pager.setVisibility(View.VISIBLE);
                go(Navigation.PeerMap);
                // i = new Intent(getApplicationContext(), MapActivity.class);
                // startActivity(i);

                break;
            case R.id.ll_message:
                Log.d(TAG, "onClick: ll_message tapped");

                // slidingTabs.setVisibility(View.VISIBLE);
                // pager.setVisibility(View.VISIBLE);
                // llCenter.setVisibility(View.GONE);

                go(Navigation.AllFeeds);

                // i = new Intent(getApplicationContext(), MessagesActivity.class);
                // startActivity(i);

                break;

            case R.id.ll_contact:
                startContactsActivity(false);
                break;

            case R.id.ll_call:
                i = new Intent(getApplicationContext(), CallLogActivity.class);
                startActivity(i);
                break;
            case R.id.ll_setting:
                dialog.show();
                cancle.setVisibility(View.VISIBLE);
                tvHeader.setText("Update Identity");
                name.setText(identities.get(0).getName());
                sid.setText(identities.get(0).subscriber.sid.toHex());
                icon.setImageDrawable(identities.get(0).getIcon());
                update.setText(R.string.identity_update);
                sidLabel.setVisibility(View.VISIBLE);
                sid.setVisibility(View.VISIBLE);
                icon.setVisibility(View.GONE);
                break;
            // Navigation n = navigation.children.get(3);
            // go(Navigation.MyFeed);
            // break;

            case R.id.alphaInstructionsContinueButton:
                Networks networks = Networks.getInstance();

                if (networks.wifiHotspot != null) {
                    networks.wifiHotspot.disable(this);
                }

                networks.wifiClient.disable(this);
                networks.blueTooth.setEnabled(true);
                networks.blueTooth.requestDiscoverable(this);
                alphaInstructionsContainer.setVisibility(View.GONE);
                break;
        }
    }

    private void init(Intent intent, Bundle savedInstanceState) {
        if ((savedInstanceState == null || savedInstanceState.isEmpty()) && intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null && extras.size() > 0) {
                // assume everything is fine...
                savedInstanceState = extras;
            }
        }

        history = NavHistory.restore(savedInstanceState);

        go();

        if (viewStack.isEmpty()) {
            throw new IllegalStateException();
        }
    }

    private Stack<ViewState> viewStack = new Stack<>();

    private boolean isStarted = false;

    private final ListObserver<Identity> idLoaded = new ListObserver<Identity>() {
        @Override
        public void added(Identity obj) {
            if (identity == null && obj.subscriber.equals(history.identity))
                go();
        }

        @Override
        public void removed(Identity obj) {

        }

        @Override
        public void updated(Identity obj) {
        }

        @Override
        public void reset() {
            go();
        }
    };

    private void popViewsTo(int offset, boolean configChange) {
        for (int j = viewStack.size() - 1; j >= offset; j--) {
            ViewState v = viewStack.get(j);
            // remove views from their containers (if required)
            IContainerView container = j > 0 ? viewStack.get(j - 1).getContainer() : this;
            container.deactivate(v, configChange, isStarted);
            viewStack.remove(j);
        }
    }

    private void go() {
        HistoryItem item = history.getTop();
        Navigation n = item.key;
        Subscriber peerSubscriber = item.peer;
        Bundle args = item.args;
        peer = null;

        Log.d(TAG, "Navigate[go] " + n.name);

        if (identity == null && history.identity != null)
            identity = serval.identities.getIdentity(history.identity);

        if (n.requiresId && identity == null && !serval.identities.isLoaded()) {
            // go() again if the identity appears
            serval.identities.listObservers.add(idLoaded);
            n = Navigation.Spinner;
            args = null;
            peerSubscriber = null;
        }

        if (identity != null)
            serval.identities.listObservers.remove(idLoaded);

        // ignore the history for now, open the identity list so a pin can be provided
        // TODO not sure if this will work right...
        // Options;
        // 1) pin entry prompt
        // 2) hide the pin in the notification
        if (n.requiresId && identity == null) {
            n = Navigation.IdentityList;
            args = null;
            peerSubscriber = null;
        }

        if (peerSubscriber != null) {
            peer = serval.knownPeers.getPeer(peerSubscriber);
        }
        if (!n.children.isEmpty())
            throw new IllegalStateException();

        Stack<Navigation> newViews = new Stack<>();
        while (n != null) {
            newViews.push(n);
            n = n.containedIn;
        }
        // ignore common parent views
        n = newViews.pop();

        int i = 0;
        while (i < viewStack.size() && viewStack.get(i).key.equals(n)) {
            i++;
            n = newViews.empty() ? null : newViews.pop();
        }

        // pop un-common views
        popViewsTo(i, false);

        // add views (& locate containers?)
        ViewState parent = viewStack.isEmpty() ? null : viewStack.get(viewStack.size() - 1);
        while (n != null) {
            IContainerView container = (parent == null) ? this : parent.getContainer();
            if (container == null)
                throw new NullPointerException();
            parent = container.activate(n, identity, peer, args, isStarted);
            if (parent == null)
                throw new NullPointerException();
            viewStack.add(parent);

            if (llCenter != null) {
                llCenter.setVisibility(View.GONE);
                slidingTabs.setVisibility(View.VISIBLE);
                pager.setVisibility(View.VISIBLE);
            }

            if (newViews.empty()) {
                break;
            }

            n = newViews.empty() ? null : newViews.pop();

        }

        rootContainer.updateToolbar(history.canGoBack());
        supportInvalidateOptionsMenu();

        View firstInput = null;
        for (ViewState state : viewStack) {
            if ((firstInput = state.getTextInput()) != null) {

                break;
            }
        }
        if (firstInput == null)
            imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
    }

    public static Intent getIntentFor(Context context, Navigation key, Identity identity, Peer peer, Bundle args) {
        // Spawn new task
        Intent intent = new Intent();
        Class<?> activity = MainActivity.class;
        int flags = Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK;
        if (identity != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                flags = Intent.FLAG_ACTIVITY_NEW_DOCUMENT
                        | Intent.FLAG_ACTIVITY_RETAIN_IN_RECENTS;
                intent.setData(Uri.parse("org.servalproject.sid:" + identity.subscriber.sid.toHex()));
            } else {
                // TODO use & persist some kind of MRU ordering?
                switch ((int) identity.getId() % 4) {
                    default:
                        activity = Id4.class;
                        break;
                    case 1:
                        activity = Id1.class;
                        break;
                    case 2:
                        activity = Id2.class;
                        break;
                    case 3:
                        activity = Id3.class;
                        break;
                }
            }
        }
        intent.addFlags(flags);
        intent.setClass(context, activity);
        intent.putExtras(NavHistory.prepareNew(
                key,
                identity == null ? null : identity.subscriber.signingKey,
                peer == null ? null : peer.getSubscriber(),
                args));
        return intent;
    }

    public void go(Navigation key, Identity identity, Peer peer, Bundle args, boolean replace) {
        if (this.identity == identity) {
            go(key, peer, args, replace);
            return;
        }
        if (replace && history.back()) {
            go();
        }
        startActivity(getIntentFor(this, key, identity, peer, args));
    }

    public void go(Navigation key) {
        go(key, false);
    }

    public void go(Navigation key, boolean replace) {
        go(key, null, null, replace);
    }

    public void go(Navigation key, Peer peer, Bundle args) {
        go(key, peer, args, false);
    }

    public void go(Navigation key, Peer peer, Bundle args, boolean replace) {
        HistoryItem item = new HistoryItem(key,
                identity == null ? null : this.identity.subscriber.signingKey,
                peer == null ? null : peer.getSubscriber(),
                args);
        go(item, replace);

    }

    public void go(HistoryItem item) {
        go(item, false);
    }

    public void go(HistoryItem item, boolean replace) {
        // record the change first, then create views
        if (history.add(item, replace)) {
            go();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        init(intent, null);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        history.save(outState);
    }

    public boolean goBack() {

        for (ViewState s : viewStack) {
            if (s.visit(new ViewState.ViewVisitor() {
                @Override
                public boolean visit(View view) {
                    return view instanceof IOnBack && ((IOnBack) view).onBack();
                }
            }))
                return true;
        }

        if (history.back()) {
            go();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, String.format("onBackPressed ChatActive: %b", chatActive));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        if (chatActive) {
            chatActive = false;
            currentPeerDoingChat = null;
            MainActivity.hideKeyboard(this);
            updateNoContactsVisibility();

            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    MainActivity.hideKeyboard(MainActivity.this);
                }
            }, 500);

            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    //chatView.presenter.onHidden();
                    //contentContainer.removeView(chatView);
                    //chatView = null;

                    ConstraintSet constraintSet = new ConstraintSet();
                    constraintSet.clone(contentParent);
                    constraintSet.connect(R.id.content_container, ConstraintSet.BOTTOM, R.id.bottom_navigation, ConstraintSet.TOP);
                    constraintSet.setVisibility(R.id.bottom_navigation, View.VISIBLE);
                    constraintSet.applyTo(contentParent);

                    actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
                    publicFeedsListView.bringToFront();
                }
            }, 1000);

            return;
        }

        if (history.history.size() == 2) {
            super.onBackPressed();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            llCenter.setVisibility(View.VISIBLE);
            appToolbar.setVisibility(View.GONE);
            MainActivity.this.finish();
        }

        if (!goBack()) {
            super.onBackPressed();
        }


        llCenter.setVisibility(View.VISIBLE);
        onUiSet();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected");

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.option_add:
                bottomNavigationView.setSelectedItemId(R.id.navigation_near_me);
                return true;
        }
        
        if (rootContainer.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    private static final int SHARE_APK = 1;

    private void shareFile(File file, String type) {
        if (App.isTesting()) {
            showSnack("Ignoring firebase testlab", Snackbar.LENGTH_SHORT);
            return;
        }
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType(type);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
            showError(e);
        }
    }

    private void alterSubscription(final MeshMBCommon.SubscriptionAction action) {
        final Peer alterPeer = peer;
        new BackgroundWorker() {
            @Override
            protected void onBackGround() throws Exception {
                identity.alterSubscription(action, alterPeer);
            }

            @Override
            protected void onComplete(Throwable t) {
                if (t != null)
                    showError(t);
                else {
                    int r = -1;
                    switch (action) {
                        case Follow:
                            r = R.string.followed;
                            break;
                        case Ignore:
                            r = R.string.ignored;
                            break;
                        case Block:
                            r = R.string.blocked;
                            break;
                    }
                    showSnack(r, Snackbar.LENGTH_SHORT);
                    supportInvalidateOptionsMenu();
                }
            }
        }.execute();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Log.d(TAG, "onMenuItemClick");

        switch (item.getItemId()) {
            case SHARE_APK:
                shareFile(serval.apkFile, "image/apk");
                return true;
            case FOLLOW:
                alterSubscription(MeshMBCommon.SubscriptionAction.Follow);
                return true;
            case IGNORE:
                alterSubscription(MeshMBCommon.SubscriptionAction.Ignore);
                return true;
            case BLOCK:
                alterSubscription(MeshMBCommon.SubscriptionAction.Block);
                return true;
            case NEARME:
                go(Navigation.PeerList);
                return true;
        }
        return false;
    }

    private void populateMenu(Menu menu, View v) {
        if (v == null)
            return;
        if (v instanceof IHaveMenu) {
            ((IHaveMenu) v).populateItems(menu);
        }
        if (v instanceof ViewGroup) {
            ViewGroup g = (ViewGroup) v;
            for (int i = 0; i < g.getChildCount(); i++)
                populateMenu(menu, g.getChildAt(i));
        }
    }

    private static final int FOLLOW = 2;
    private static final int IGNORE = 3;
    private static final int BLOCK = 4;
    private static final int NEARME = 5;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (true) {
            return true;
        }


        if (!App.isTesting()) {
//			Log.e("San","here near menu ");

//			MenuItem item = menu.add(Menu.NONE, NEARME, Menu.NONE, R.string.near_me)
//					.setOnMenuItemClickListener(this)
//					.setIcon(R.drawable.near_me);
//			MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        }

        if (identity != null && peer != null && peer.getSubscriber().signingKey != null) {
            Messaging.SubscriptionState state = identity.messaging.getSubscriptionState(peer.getSubscriber());

            if (state != null) {
//				Log.e("San","pager size: " + pager.getChildCount());
                pager.requestDisallowInterceptTouchEvent(true);

                pager.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });

//				if (state != Messaging.SubscriptionState.Ignored) {
//					MenuItem item = menu.add(Menu.NONE, IGNORE, Menu.NONE, R.string.ignore_feed)
//							.setOnMenuItemClickListener(this)
//							.setIcon(R.drawable.ic_remove_contact);
//					MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
//				}
//				if (state != Messaging.SubscriptionState.Followed) {
//					MenuItem item = menu.add(Menu.NONE, FOLLOW, Menu.NONE, R.string.follow_feed)
//							.setOnMenuItemClickListener(this)
//							.setIcon(R.drawable.ic_add_contact);
//					MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
//				}
//				if (state != Messaging.SubscriptionState.Blocked) {
//					MenuItem item = menu.add(Menu.NONE, BLOCK, Menu.NONE, R.string.block_contact)
//							.setOnMenuItemClickListener(this)
//							.setIcon(R.drawable.ic_block_contact);
//					MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_NEVER);
//				}
            }
        } else {
            MenuItem item = menu.add(Menu.NONE, NEARME, Menu.NONE, R.string.near_me)
                    .setOnMenuItemClickListener(this)
                    .setIcon(R.drawable.near_me);
            MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        }
        populateMenu(menu, viewStack.peek().view);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (bottomNavigationView.getSelectedItemId() == R.id.navigation_chats && !startingUp && chatView != null) {
            getMenuInflater().inflate(R.menu.menu_add, menu);
        }
        else {
            appToolbar.getMenu().clear();
        }
        return true;
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
        isStarted = false;
        for (ViewState state : viewStack) {
            ILifecycle lifecycle = state.getLifecycle();
            if (lifecycle != null)
                lifecycle.onHidden();
        }
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        isStarted = true;
        for (ViewState state : viewStack) {
            ILifecycle lifecycle = state.getLifecycle();
            if (lifecycle != null)
                lifecycle.onVisible();
        }
        super.onStart();
        changingConfig = false;
    }

    private boolean changingConfig = false;

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        changingConfig = true;
        return super.onRetainCustomNonConfigurationInstance();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        if (Build.VERSION.SDK_INT >= 11)
            changingConfig = isChangingConfigurations();
        popViewsTo(0, changingConfig);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void deactivate(ViewState state, boolean configChange, boolean visible) {
        ILifecycle lifecycle = state.getLifecycle();
        if (visible && lifecycle != null)
            lifecycle.onHidden();
        if (lifecycle != null)
            lifecycle.onDetach(configChange);
        ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        root.removeView(state.view);
    }

    @Override
    public ViewState activate(Navigation n, Identity identity, Peer peer, Bundle args, boolean visible) {
        Log.d(TAG, "Navigation[activate] " + n.name);

        ViewState ret = ViewState.Inflate(this, n, identity, peer, args);

        if (!ret.visit(new ViewState.ViewVisitor() {
            @Override
            public boolean visit(View view) {
                if (view instanceof IRootContainer)
                    rootContainer = (IRootContainer) view;
                return rootContainer != null;
            }
        }))
            throw new IllegalStateException();

        ret.view.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        setContentView(ret.view);

        try {
            if (pager.getChildCount() > 2)
                pager.setCurrentItem(2);
        } catch (Exception e) {
           // e.printStackTrace();
        }

        ILifecycle lifecycle = ret.getLifecycle();
        if (visible && lifecycle != null)
            lifecycle.onVisible();
        return ret;
    }

    @NonNull
    @Override
    public ActionBar getSupportActionBar() {
        ActionBar ret = super.getSupportActionBar();
        //if (ret == null)
        //	throw new IllegalStateException();
        return ret;
    }

    private class CrashReportException extends RuntimeException {
        CrashReportException(Throwable e) {
            super(e);
        }
    }

    public void showError(final Throwable e) {
        if (App.isTesting())
            throw new CrashReportException(e);
        showSnack(e.getMessage(), Snackbar.LENGTH_LONG, getString(R.string.email_log),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent i = app.getErrorIntent(e);
                        startActivity(i);
                    }
                });
    }

    public void showSnack(CharSequence message, int length) {
        this.showSnack(message, length, null, null);
    }

    public void showSnack(int messageRes, int length) {
        this.showSnack(getString(messageRes), length, null, null);
    }

    public void showSnack(CharSequence message, int length, CharSequence actionLabel, View.OnClickListener action) {
        Snackbar s = Snackbar.make(rootContainer.getCoordinator(), message, length);
        if (action != null && actionLabel != null)
            s.setAction(actionLabel, action);
        s.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == 120) {
                if (PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG)
                        == PackageManager.PERMISSION_GRANTED) {
//                    Log.e("hello", " read call log enabled");
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS},
                            130);
                } else if (PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG)
                        == PackageManager.PERMISSION_DENIED) {
                    MainActivity.this.finish();
                }
            } else if (requestCode == 130) {
                if (PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    // Log.e("hello", " read contacts enabled");
                    //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 140);
                    onContactsPermissionGranted();
                } else if (PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                        == PackageManager.PERMISSION_DENIED) {
                    MainActivity.this.finish();
                }
            } else if (requestCode == 140) {
                if (PermissionChecker.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                        == PackageManager.PERMISSION_GRANTED) {
//                    Log.e("hello", " call contacts enabled");
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CALL_LOG},
                            150);
                } else if (PermissionChecker.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                        == PackageManager.PERMISSION_DENIED) {
                    MainActivity.this.finish();
                }

            } else if (requestCode == 150) {
                if (PermissionChecker.checkSelfPermission(this, Manifest.permission.WRITE_CALL_LOG)
                        == PackageManager.PERMISSION_GRANTED) {
                    newIdentity();
//                    Log.e("hello", " call contacts enabled");
                } else if (PermissionChecker.checkSelfPermission(this, Manifest.permission.WRITE_CALL_LOG)
                        == PackageManager.PERMISSION_DENIED) {
                    MainActivity.this.finish();
                }

            } else {

            }

        } else {
//			Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
//            /MainActivity.this.finish();
        }

    }

    private void openContentView(int option) {
        switch (option) {
            case R.id.ll_message:
                publicFeedsListView.bringToFront();
                setTitle(R.string.chats);
                break;

            case R.id.navigation_near_me:
                peerListView.bringToFront();
                setTitle(R.string.mesh_chat);
                break;
        }

        chatActive = false;
        invalidateOptionsMenu();
        updateNoContactsVisibility();
    }

    private void createContentViews() {
        publicFeedsListView = LayoutInflater.from(this).inflate(R.layout.feed_list, contentContainer, true).findViewById(R.id.public_feed_list);
        publicFeedsListView.onAttach(this, null, identity, null, null);
        publicFeedsListView.presenter.onVisible();

        peerListView = LayoutInflater.from(this).inflate(R.layout.peer_list, contentContainer, true).findViewById(R.id.peer_list);
        peerListView.onVisible();

        chatView = LayoutInflater.from(MainActivity.this).inflate(R.layout.message_list, contentContainer, true).findViewById(R.id.private_message_list);
    }

    private void createContentView(int option) {
        if (contentView != null) {
            if (contentView instanceof PublicFeedsList) {
                PublicFeedsList publicFeedsList = (PublicFeedsList) contentView;
                //publicFeedsList.presenter.onHidden();
                //publicFeedsList.presenter.onDestroy();
                //publicFeedsList.presenter = null;
            }
            contentContainer.removeView(contentView);
        }

        Fragment fragment = null;

        switch (option) {
            case R.id.ll_message:
                Log.d(TAG, String.format("createContentView: chats. Rechable peers count: %d", serval.knownPeers.getReachablePeers().size()));
                PublicFeedsList publicFeedsList = LayoutInflater.from(this).inflate(R.layout.feed_list, contentContainer, true).findViewById(R.id.public_feed_list);
                publicFeedsList.onAttach(this, null, identity, null, null);
                publicFeedsList.presenter.onVisible();
                contentView = publicFeedsList;
                setTitle(R.string.chats);
                break;

            case R.id.ll_contact:
                Log.d(TAG, "createContentView: contact");
                fragment = new ContactFragment();
                setTitle(R.string.contacts);
                break;

            case R.id.navigation_near_me:
                Log.d(TAG, "createContentView: near me");
                PeerList peerList = LayoutInflater.from(this).inflate(R.layout.peer_list, contentContainer, true).findViewById(R.id.list);
                peerList.onVisible();
                contentView = peerList;
                setTitle(R.string.mesh_chat);
                break;
                
            default:
                Log.d(TAG, "createContentView: unknown");
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_container, fragment).commit();
        }

        invalidateOptionsMenu();
        updateNoContactsVisibility();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void event(Events.BottomNavigationSelected event) {
        openContentView(event.option);
        //createContentView(event.option);
        //onClick(event.option);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void event(final Events.ChatSelected event) {
        Log.d(TAG, "chat");
        currentPeerDoingChat = event.peer;

        long delay = 10;
        if (bottomNavigationView.getSelectedItemId() != R.id.navigation_chats) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_chats);
            delay = 500;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                chatView.onAttach(MainActivity.this, null, identity, event.peer, null);
                chatView.presenter.onVisible();
                chatView.bringToFront();
                chatActive = true;
                updateNoContactsVisibility();

                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(contentParent);
                constraintSet.connect(R.id.content_container, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
                constraintSet.setVisibility(R.id.bottom_navigation, View.INVISIBLE);
                constraintSet.applyTo(contentParent);

                actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                actionBarDrawerToggle.syncState();
            }
        }, delay);

        chatActive = true;
        invalidateOptionsMenu();
        updateNoContactsVisibility();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void event(Events.FinishMainActivity event) {
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void event(Events.FeedAdded event) {
        Log.d(TAG, String.format("feedAdded. isChatter: %b", event.isChatter));
        meshContacts.add(event.item);
        if (event.isChatter) {
            hasFeeds = true;
            updateNoContactsVisibility();
            if (publicFeedsListView != null) {
                publicFeedsListView.presenter.adapter.added(event.item);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Log.d(TAG, "onSupportNavigateUp");
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onNavigateUp() {
        Log.d(TAG, "onNavigateUp");
        return super.onNavigateUp();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Log.d(TAG, "onNavigationItemSelected");

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;

        switch (id) {
            case R.id.navigation_bluetooth:
                Intent i = new Intent(getApplicationContext(), ConnectActivity.class);
                startActivity(i);
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_container, fragment).commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void updateNoContactsVisibility() {
        //Log.d(TAG, String.format("updateNoContactsVisibility: currentOption: %d navigation_chats: %d", bottomNavigationListener.getCurrentOption(), R.id.navigation_chats));
        noContactsTextView.setVisibility((startingUp || hasFeeds || bottomNavigationListener.getCurrentOption() != R.id.navigation_chats || chatActive) ? View.GONE : View.VISIBLE);
    }

    private SharedPreferences getSharedPrerences() {
        return getApplicationContext().getSharedPreferences(SharedPrefsKeys.CHATTERS, 0);
    }

    private SharedPreferences.Editor getSharedPrederencesEditor() {
        return getSharedPrerences().edit();
    }

    private HashSet<String> getChatters() {
        String value = getSharedPrerences().getString(SharedPrefsKeys.CHATTERS, null);
        HashSet<String> chatters;

        if (value == null) {
            chatters = new HashSet<>();
        }
        else {
            chatters = new Gson().fromJson(value, new TypeToken<HashSet<String>>(){}.getType());
        }

        for (String chatter : chatters) {
            Log.d(TAG, "getChatters: " + chatter);
        }

        return chatters;
    }

    public synchronized void addPeerAsChatter(String peerId) {
        Log.d(TAG, String.format("addPeerAsChatter %s", peerId));
        HashSet<String> chatters = getChatters();
        chatters.add(peerId);

        SharedPreferences.Editor editor = getSharedPrederencesEditor();
        editor.putString(SharedPrefsKeys.CHATTERS, new Gson().toJson(chatters));
        editor.apply();

        /*
        final FeedListAdapter feedListAdapter = PublicFeedsPresenter.adapter;
        Log.d(TAG, "addPeerAsChatter: AKI");

        if (feedListAdapter != null) {
            final long itemDelay = 100;
            final long totalDelay = 100 * meshContacts.size();

            serval.backgroundHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        for (final RhizomeListBundle obj : meshContacts) {
                            //serval.backgroundHandler.postDelayed(new Runnable() {
                            //    @Override
                            //    public void run() {
                                    feedListAdapter.added(obj);
                            //    }}, itemDelay);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    feedListAdapter.notifyDataSetChanged();
                    if (currentPeerDoingChat != null && chatView != null) {
                        chatView.presenter.adapter.notifyDataSetChanged();
                        //chatView.onAttach(MainActivity.this, null, identity, currentPeerDoingChat, null);
                        //chatView.presenter.onVisible();
                    }
                }
            }, totalDelay);
        }
        */
    }

    public void addPeerAsChatter(Peer peer) {
        addPeerAsChatter(peer.getSubscriber().sid.toString());
    }

    public boolean isChatter(RhizomeListBundle item) {
        String id = item.author.toString();
        boolean result = getChatters().contains(id);
        Log.d(TAG, String.format("isChatter: %s %b", id, result));
        return result;
    }
}
