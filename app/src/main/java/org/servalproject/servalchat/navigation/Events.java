package org.servalproject.servalchat.navigation;

import org.servalproject.mid.Peer;
import org.servalproject.servaldna.rhizome.RhizomeListBundle;

/**
 * Created by rodrigobrauwers on 14/02/18.
 */

public class Events {

    public static class BottomNavigationSelected {
        int option;

        public BottomNavigationSelected(int option) {
            this.option = option;
        }
    }

    public static class ChatSelected {
        Peer peer;

        public ChatSelected(Peer peer) {
            this.peer = peer;
        }
    }

    public static class FinishMainActivity { }

    public static class FeedAdded {
        RhizomeListBundle item;
        boolean isChatter;

        public FeedAdded(RhizomeListBundle item, boolean isChatter) {
            this.item = item;
            this.isChatter = isChatter;
        }
    }

}
