package org.servalproject.servalchat;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.servalproject.mid.FeedList;
import org.servalproject.mid.Identity;
import org.servalproject.mid.IdentityFeed;
import org.servalproject.mid.Peer;
import org.servalproject.mid.Serval;
import org.servalproject.servalchat.feeds.FeedListAdapter;
import org.servalproject.servalchat.feeds.PublicFeedsList;
import org.servalproject.servalchat.navigation.MainActivity;
import org.servalproject.servalchat.navigation.Navigation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by falcon on 13/12/17.
 */

public class MessagesActivity extends AppCompatActivity {
    ImageView imgBack;
    TextView txtTitle;
    LinearLayout llTop;
    private Serval serval;
    private Identity identity;
    private Peer peer;
    private InputMethodManager imm;
    private List<Identity> identities =new ArrayList<>();
    IdentityAdapter adapter;
    private IdentityFeed feed;
    MainActivity mainActivity;
    Context context;
    PublicFeedsList publicFeedsList;
//    public MessagesActivity(Context cont){
//        this.context = cont;
//    }
//
//    public MessagesActivity(){
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msg);

        double ww = MainActivity.width/3;
        double hh = MainActivity.height/5;

        imgBack = (ImageView) findViewById(R.id.img_more);
        txtTitle = (TextView)findViewById(R.id.txt_title);
        llTop = (LinearLayout)findViewById(R.id.ll_top);
        publicFeedsList = (PublicFeedsList) findViewById(R.id.list);
        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,((int)hh/7)*3);
        llTop.setLayoutParams(parms);

        RelativeLayout.LayoutParams parmsImgMore = new RelativeLayout.LayoutParams((int)hh/3,(int)hh/3);
        imgBack.setPadding((int)hh/13,0,0,0);
        imgBack.setLayoutParams(parmsImgMore);
        txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)hh/5);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessagesActivity.this.finish();

            }
        });

//        mainActivity =(MainActivity) App.getContext();
//        Navigation n = navigation.children.get(3);
//				go(Navigation.MyFeed);

//        serval = Serval.getInstance();
//        identities = serval.identities.getIdentities();
//
//        FeedList feedList = identity.getAllFeeds();
//        mainActivity = (MainActivity) context;

//        Log.e("San feedlist : ",mainActivity+"");
//        Log.e("San feedlist : ",mainActivity.feedList + "");
//        adapter = new IdentityAdapter(mainActivity.feedList, this);
//        publicFeedsList.setAdapter(adapter);

    }


    public void showError(Throwable t) {
    }
}