package org.servalproject.servalchat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import org.servalproject.SharedPrefsKeys;
import org.servalproject.servalchat.navigation.MainActivity;
import org.servalproject.servalchat.views.GetStartedActivity;

import agency.tango.android.avatarview.utils.StringUtils;

/**
 * Created by falcon on 13/12/17.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences pref = getApplicationContext().getSharedPreferences(SharedPrefsKeys.IDENTITY_PREFS, 0);
        String identityName = pref.getString("identity_name", null);

        if (StringUtils.isNullOrEmpty(identityName)) {
            startGetStartedActivity();
        }
        else {
            setContentView(R.layout.activity_splash);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }
            }, 2000);
        }
    }

    private void startGetStartedActivity() {
        Intent i = new Intent(getApplicationContext(), GetStartedActivity.class);
        startActivity(i);
    }

}