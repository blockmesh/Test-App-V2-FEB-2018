package org.servalproject.servalchat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.servalproject.servalchat.navigation.MainActivity;

/**
 * Created by falcon on 13/12/17.
 */

public class MapActivity extends AppCompatActivity {
    ImageView imgBack;
    TextView txtTitle;
    LinearLayout llTop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        double ww = MainActivity.width/3;
        double hh = MainActivity.height/5;

        imgBack = (ImageView) findViewById(R.id.img_more);
        txtTitle = (TextView)findViewById(R.id.txt_title);
        llTop = (LinearLayout)findViewById(R.id.ll_top);

        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,((int)hh/7)*3);
        llTop.setLayoutParams(parms);

        RelativeLayout.LayoutParams parmsImgMore = new RelativeLayout.LayoutParams((int)hh/3,(int)hh/3);
        imgBack.setPadding((int)hh/13,0,0,0);
        imgBack.setLayoutParams(parmsImgMore);
        txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)hh/5);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapActivity.this.finish();

            }
        });
    }



}