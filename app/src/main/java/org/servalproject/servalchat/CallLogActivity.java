package org.servalproject.servalchat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.servalproject.servalchat.navigation.MainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by falcon on 13/12/17.
 */

public class CallLogActivity extends AppCompatActivity {
    ImageView imgBack;
    TextView txtTitle;
    LinearLayout llTop;
    ListView listCallLog;
    private ArrayList<CallLogItemList> callLogItemLists = new ArrayList<CallLogItemList>();
    private CallLogAdapter callLogAdapter;

    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        progressDialog = new ProgressDialog(CallLogActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        double ww = MainActivity.width/3;
        double hh = MainActivity.height/5;

        imgBack = (ImageView) findViewById(R.id.img_more);
        txtTitle = (TextView)findViewById(R.id.txt_title);
        llTop = (LinearLayout)findViewById(R.id.ll_top);
        listCallLog = (ListView) findViewById(R.id.list_call_log);

        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,((int)hh/7)*3);
        llTop.setLayoutParams(parms);

        RelativeLayout.LayoutParams parmsImgMore = new RelativeLayout.LayoutParams((int)hh/3,(int)hh/3);
        imgBack.setPadding((int)hh/13,0,0,0);
        imgBack.setLayoutParams(parmsImgMore);
        parmsImgMore.addRule(RelativeLayout.CENTER_VERTICAL);
        txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)hh/5);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallLogActivity.this.finish();

            }
        });



//        if (progressDialog == null) {
            // in standard case YourActivity.this


//        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getCallLog();
            }
        });
        thread.start();



        listCallLog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String number = callLogItemLists.get(position).getPhNumber();

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+number));
                try {
                    startActivity(callIntent);
                }
                catch (SecurityException se)
                {

                }catch (Exception e){}
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();


    }

    public void getCallLog() {
        Uri allCalls = Uri.parse("content://call_log/calls");
         Cursor managedCursor = getApplicationContext().getContentResolver().query(allCalls, null, null, null, CallLog.Calls.DATE + " DESC");


//        Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null,null, null, null);

        int id = managedCursor.getColumnIndex(CallLog.Calls._ID);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
//        int nameColumn = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
//        int nameColumn = managedCursor.getColumnIndex(CallLog.Calls.);
//        Log.e("San ", "id: " + id+" - "+number);

        while (managedCursor.moveToNext()) {

            CallLogItemList callLogItemList = new CallLogItemList();
            int call_id = managedCursor.getInt(id);
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            String contactName = getContactName(getApplicationContext(),phNumber);






            Date callDayTime = new Date(Long.valueOf(callDate));

            SimpleDateFormat dateFormatGmt = new SimpleDateFormat("dd/MM HH:mm:ss");
            dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
            String dates = dateFormatGmt.format(callDayTime)+"";


            String callDuration = managedCursor.getString(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
//            Log.e("San " , phNumber +" - "+dir+" -"+callDuration+" - "+dates+" - "+ contactName);

//
            callLogItemList.setName(contactName);
            callLogItemList.setPhNumber(phNumber);
            callLogItemList.setDir(dir);
            callLogItemList.setCallDayTime(dates);
            callLogItemList.setCallDuration(callDuration);

            callLogItemLists.add(callLogItemList);

        }

        CallLogActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                callLogAdapter  = new CallLogAdapter(getApplicationContext(),callLogItemLists,false);
                callLogAdapter.notifyDataSetChanged();
                listCallLog.setAdapter(callLogAdapter);
            }
        });



        try{
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }catch(Exception exception){
            exception.printStackTrace();
        }

    }


    public String getContactName(Context context,final String phoneNumber)
    {
        Uri uri;
        String[] projection;
        Uri mBaseUri = Contacts.Phones.CONTENT_FILTER_URL;
        projection = new String[] { android.provider.Contacts.People.NAME };
        try {
            Class<?> c =Class.forName("android.provider.ContactsContract$PhoneLookup");
            mBaseUri = (Uri) c.getField("CONTENT_FILTER_URI").get(mBaseUri);
            projection = new String[] { "display_name" };
        }
        catch (Exception e) {
        }


        uri = Uri.withAppendedPath(mBaseUri, Uri.encode(phoneNumber));
        Cursor cursor =context.getContentResolver().query(uri, projection, null, null, null);

        String contactName = "Unknow";

        if (cursor.moveToFirst())
        {
            contactName = cursor.getString(0);
        }

        cursor.close();
        cursor = null;
//        Log.e("San contactName:-",contactName+" ");
        return contactName;
    }

}