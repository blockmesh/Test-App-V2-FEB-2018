package org.servalproject;

/**
 * Created by rodrigobrauwers on 16/02/18.
 */

public class SharedPrefsKeys {

    public final static String IDENTITY_PREFS = "IdentityPref";
    public final static String IDENTITY_NAME_TMP = "IdentityNameTmp";
    public final static String CHATTERS = "Chatters";

}
